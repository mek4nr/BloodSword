package com.unidev.bloodsword.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by jlidou on 2017-03-20.
 */

public class ConnexionBDD {

    private static SQLiteDatabase bd;
    private static String database = "BLOODSWORD_DATABASE";
    private static int version = 1;


    public static SQLiteDatabase getBDD(Context ctx) {
        DBHelper pH = new DBHelper(ctx, database, null, version);
        return bd = pH.getWritableDatabase();
    }

    public static void close() {
        bd.close();
    }

    public static void deleteBd(Context ctx) {
        ctx.deleteDatabase(database);
        Log.d("bd", "bd drop");
    }


    /**
     créer un repertoire src/assets dans votre projet
     deposez le fichier de la base de donné du meme nom que l attribut database
     mais sans le ".db" a la racine du repertoire
     utilisez cette methode et votre base de donnée sera importé

     */

    public static void importBdd(Context ctx) {
        AssetManager assetManager = ctx.getAssets();
        InputStream inputStream = null;
        OutputStream outputStream = null;

        try {
            inputStream = assetManager.open(database);
            outputStream = new FileOutputStream(new File("" + ctx.getDatabasePath(database).getAbsolutePath()));
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
            Log.d("bd", "bd import");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (outputStream != null) {
                try {
                    // outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     * @return true if it exists, false if it doesn't
     */
    public static boolean checkDataBase(Context ctx){

        SQLiteDatabase checkDB = null;
        String DB_PATH = "/data/data/" + ctx.getPackageName() + "/databases/";

        try{
            String myPath = DB_PATH + database;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

        }catch(SQLiteException e){

            //database does't exist yet.

        }

        if(checkDB != null){

            checkDB.close();

        }

        return checkDB != null ? true : false;
    }


}
