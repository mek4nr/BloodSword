package com.unidev.bloodsword.utils;

public class Die {

    public static int diceFace = 6;

    public static int roll(int diceNumber) {
        int  result = 0;
        for(int i=0; i<diceNumber; i++){
            result += 1 + ((int) (Math.random()*diceFace));
        }

        return result;
    }

}