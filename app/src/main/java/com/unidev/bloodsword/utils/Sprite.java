package com.unidev.bloodsword.utils;

import java.io.Serializable;

public class Sprite implements Serializable{

    protected static final long serialVersionUID = 39L;
    int[] resImages;
    int indexIddle;

    public Sprite(int[] resImages, int indexIddle) {
        this.resImages = resImages;
        this.indexIddle = indexIddle;
    }

    public Sprite(int resImage) {
        this.resImages = new int[1];
        this.resImages[0] = resImage;
        this.indexIddle = 0;
    }

    public int getImageIddle(){
        return this.resImages[indexIddle];
    }
}
