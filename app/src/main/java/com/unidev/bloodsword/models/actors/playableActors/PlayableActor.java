package com.unidev.bloodsword.models.actors.playableActors;

import android.content.Context;
import android.support.annotation.DrawableRes;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.managers.RankManager;
import com.unidev.bloodsword.models.actors.Actor;
import com.unidev.bloodsword.models.actors.EActorStat;
import com.unidev.bloodsword.models.actors.ECharacterType;
import com.unidev.bloodsword.models.battle.EnemyTile;
import com.unidev.bloodsword.models.battle.Tile;
import com.unidev.bloodsword.models.battle.WallTile;
import com.unidev.bloodsword.models.inventory.EquipableItem;
import com.unidev.bloodsword.models.inventory.Inventory;
import com.unidev.bloodsword.models.inventory.Pouch;
import com.unidev.bloodsword.models.inventory.UseableItem;
import com.unidev.bloodsword.models.inventory.amorItems.ArmourItem;
import com.unidev.bloodsword.models.inventory.weaponItems.WeaponItem;
import com.unidev.bloodsword.utils.Die;
import com.unidev.bloodsword.utils.Sprite;

public abstract class PlayableActor extends Actor{

    protected int experience;
    protected int experiencePointsBonus;
    protected ECharacterType characterType;
    protected Inventory inventory;

    public PlayableActor(Integer fightingProwess, Integer damageDice, Integer damageBonus, Integer psychicAbility, Integer awareness, Integer endurance) {
        super(fightingProwess, damageDice, damageBonus, psychicAbility, awareness, endurance);
    }

    public PlayableActor() {
    }

    public PlayableActor(String name, Sprite sprite, ECharacterType characterType, int rank, Context context) {
        super(name, sprite);
        this.characterType = characterType;
        inventory = new Inventory();
        newGame(context, characterType, rank);
        experience = 250;
        experiencePointsBonus = 0;
    }

    public void newGame(Context context, ECharacterType type, int rank){
        this.newGameStats(RankManager.getStatsFromRank(context,type,rank));
        inventory.clear();
        inventory.add(new Pouch(rank*5));
        this.newGameInventory(type);
    }

    public void equip(EquipableItem item){
        EquipableItem equipableItem = null;
        switch(item.getSlot()){
            case ARMOUR:
                equipableItem = this.armour;
                this.armour = (ArmourItem) item;
                break;
            case WEAPON:
                equipableItem = this.weapon;
                this.weapon = (WeaponItem) item;
                break;
        }

        if(equipableItem != null)
            this.inventory.add(equipableItem);
    }

    public void use(UseableItem item){
        item.use(this);
    }

    public abstract void newBattle();

    @Override
    public boolean canMove(Tile tile) {
        return (!(tile instanceof EnemyTile) && !(tile instanceof WallTile) && (tile.getActor() == null || tile.getActor() == this));
    }

    public abstract void newGameInventory(ECharacterType type);

    public ECharacterType getCharacterType() {
        return characterType;
    }

    public @DrawableRes int getRessourceImage(){
        int result = 0;

        switch (getCharacterType()){
            case WARRIOR:
                result = R.drawable.portrait_warrior;
                break;
            case TRICKSTER:
                result = R.drawable.portrait_trickster;
                break;
            case SAGE:
                result = R.drawable.portrait_sage;
                break;
            case ENCHANTER:
                result = R.drawable.portrait_enchanter;
                break;
        }
        return result;
    }
    public static String getDefaultName() {
        return "Grodu";
    }

    public Boolean testStat(EActorStat stat, int diceNb) {

        Boolean result = false;
        int characteristicToCheck = 0;
        int dieRoll = Die.roll(diceNb);
        switch(stat) {
            case FIGHTING_PROWESS:
                characteristicToCheck = getFightingProwess();
                break;
            case  PSYCHIC_ABILITY:
                characteristicToCheck = getPsychicAbility();
                break;
            case AWARENESS:
                characteristicToCheck = getAwareness();
        }

        if(dieRoll <= characteristicToCheck) {
            result = true;
        } else {
            result = false;
        }

        return result;
    }

    public Inventory getInventory() {
        return inventory;
    }
}