package com.unidev.bloodsword.models.actors.enemyActors;

import com.unidev.bloodsword.models.actors.Actor;
import com.unidev.bloodsword.models.battle.EnemyTile;
import com.unidev.bloodsword.models.battle.Tile;
import com.unidev.bloodsword.models.battle.WallTile;
import com.unidev.bloodsword.models.inventory.amorItems.ArmourItem;
import com.unidev.bloodsword.models.inventory.weaponItems.WeaponItem;
import com.unidev.bloodsword.utils.Sprite;

public abstract class EnemyActor extends Actor {
    public EnemyActor(String name, Sprite sprite, int fightingProwess, int damageDice, int damageBonus, int psychicAbility, int awareness, int endurance, ArmourItem armour, WeaponItem weapon) {
        super(name, sprite, fightingProwess, damageDice, damageBonus, psychicAbility, awareness, endurance, armour, weapon);
    }

    @Override
    public boolean canMove(Tile tile) {
        return (!(tile instanceof WallTile) && (tile.getActor() == null || tile.getActor() == this));
    }
}