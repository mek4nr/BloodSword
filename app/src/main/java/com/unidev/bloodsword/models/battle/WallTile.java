package com.unidev.bloodsword.models.battle;

import android.media.Image;

import com.unidev.bloodsword.models.actors.Actor;


public class WallTile extends Tile{
    public WallTile(int posX, int posY) {
        super(posX, posY, null, null, false, false, false);
    }
}
