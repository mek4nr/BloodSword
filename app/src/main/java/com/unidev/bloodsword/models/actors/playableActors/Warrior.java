package com.unidev.bloodsword.models.actors.playableActors;

import android.content.Context;
import android.support.annotation.DrawableRes;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.models.actors.ECharacterType;
import com.unidev.bloodsword.models.inventory.amorItems.ArmourItem;
import com.unidev.bloodsword.models.inventory.weaponItems.WeaponItem;
import com.unidev.bloodsword.utils.Sprite;

public class Warrior extends PlayableActor {

    public Warrior(String name, int rank, Context context) {
        super(name, new Sprite(R.drawable.warrior), ECharacterType.WARRIOR, rank, context);
    }

    public Warrior(Integer fightingProwess, Integer damageDice, Integer damageBonus, Integer psychicAbility, Integer awereness, Integer endurance) {
        super(fightingProwess, damageDice, damageBonus, psychicAbility, awereness, endurance);
    }

    @Override
    public void newBattle() {

    }

    @Override
    public void newGameInventory(ECharacterType type) {
        this.weapon = new WeaponItem("sword", 0, 0);
        this.armour = new ArmourItem("chainmail armour", 0, 3);
    }

    public static @DrawableRes int getImageResource(){
        return R.drawable.portrait_warrior;
    }
    public static String getDefaultName() {
        return "Grodu";
    }
}