package com.unidev.bloodsword.models.spells;

import com.unidev.bloodsword.models.actors.Actor;

import java.io.Serializable;
import java.util.ArrayList;

public abstract class Spell implements Serializable{

    protected static final long serialVersionUID = 48L;
    protected String name;
    protected String description;
    protected int complexity;
    protected int maxTarget;
    protected int damageDice;
    protected int damageBonus;
    protected boolean bActiveArmour;


    public Spell(String name, int complexity, int damageDice, int damageBonus, boolean bActiveArmour) {
        this.name = name;
        this.complexity = complexity;
        this.damageDice = damageDice;
        this.damageBonus = damageBonus;
        this.bActiveArmour = bActiveArmour;
    }

    public Spell(String name, int complexity, int maxTarget, int damageDice, int damageBonus, boolean bActiveArmour) {
        this.name = name;
        this.complexity = complexity;
        this.maxTarget = maxTarget;
        this.damageDice = damageDice;
        this.damageBonus = damageBonus;
        this.bActiveArmour = bActiveArmour;
    }

    public boolean needTarget(){
        return maxTarget > 0;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getComplexity() {
        return complexity;
    }

    public int getMaxTarget() {
        return maxTarget;
    }

    public int getDamageDice() {
        return damageDice;
    }

    public int getDamageBonus() {
        return damageBonus;
    }

    public boolean isActiveArmour() {
        return bActiveArmour;
    }
}