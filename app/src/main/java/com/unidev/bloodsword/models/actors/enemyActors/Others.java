package com.unidev.bloodsword.models.actors.enemyActors;

import com.unidev.bloodsword.models.inventory.amorItems.ArmourItem;
import com.unidev.bloodsword.models.inventory.weaponItems.WeaponItem;
import com.unidev.bloodsword.utils.Sprite;

public class Others extends EnemyActor {
    public Others(String name, Sprite sprite, int fightingProwess, int damageDice, int damageBonus, int psychicAbility, int awereness, int endurance, ArmourItem armour, WeaponItem weapon) {
        super(name, sprite, fightingProwess, damageDice, damageBonus, psychicAbility, awereness, endurance, armour, weapon);
    }
}