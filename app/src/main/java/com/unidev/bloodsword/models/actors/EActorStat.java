package com.unidev.bloodsword.models.actors;

public enum EActorStat {
    FIGHTING_PROWESS("fightingProwess"),
    PSYCHIC_ABILITY("psychicAbility"),
    AWARENESS("awereness"),
    ENDURANCE("endurance");

    private String statName;

    EActorStat(String statName) {
        this.statName = statName;
    }
}
