package com.unidev.bloodsword.models.actors;

import com.unidev.bloodsword.models.spells.Spell;

import java.util.ArrayList;
import java.util.List;

public interface SpellCasterActor {

    public ArrayList<Spell> getSpells();

    public ArrayList<Spell> getMemorySpells();

    public boolean storeSpell(Spell spell);

    public int useSpell(List<Actor> targets, Spell spell, int turn);

    public boolean rollPsychicTest(Spell spell, int turn);

    public int launchSpell(List<Actor> targets, Spell spell);

    public int canUseSpell(Spell spell);

    public int canStoreSpell(Spell spell);

}