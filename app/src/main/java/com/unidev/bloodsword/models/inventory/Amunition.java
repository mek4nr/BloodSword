package com.unidev.bloodsword.models.inventory;

import android.media.Image;
import android.support.annotation.DrawableRes;

public class Amunition extends StorageItemItem {
    public Amunition(String name, @DrawableRes int image) {
        super(name, image);
    }
}