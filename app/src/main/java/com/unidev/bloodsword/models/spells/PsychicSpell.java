package com.unidev.bloodsword.models.spells;

import com.unidev.bloodsword.models.actors.Actor;

import java.util.ArrayList;

public class PsychicSpell extends Spell {

    protected boolean resistable;

    public PsychicSpell(String name, int complexity, int damageDice, int damageBonus, boolean bActiveArmour, boolean resistable) {
        super(name, complexity, damageDice, damageBonus, bActiveArmour);
        this.resistable = resistable;
    }
}