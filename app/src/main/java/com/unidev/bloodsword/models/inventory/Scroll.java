package com.unidev.bloodsword.models.inventory;

import android.media.Image;
import android.support.annotation.DrawableRes;

import com.unidev.bloodsword.utils.Sound;

public class Scroll extends UseableItem {
    public Scroll(String name, @DrawableRes int image, Sound useSound) {
        super(name, image, useSound);
    }
}