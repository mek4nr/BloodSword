package com.unidev.bloodsword.models.actors.enemyActors;

import com.unidev.bloodsword.models.inventory.amorItems.ArmourItem;
import com.unidev.bloodsword.models.inventory.weaponItems.WeaponItem;
import com.unidev.bloodsword.utils.Sprite;

public class ManInBlue extends EnemyActor {
    public ManInBlue(int fightingProwess, int damageDice, int damageBonus, int psychicAbility, int awareness, int endurance, ArmourItem armour, WeaponItem weapon) {
        super("Man in Blue", null, fightingProwess, damageDice, damageBonus, psychicAbility, awareness, endurance, armour, weapon);
    }
}
