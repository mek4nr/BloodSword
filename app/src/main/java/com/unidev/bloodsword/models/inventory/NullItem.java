package com.unidev.bloodsword.models.inventory;

public class NullItem extends Item {
    public NullItem() {
        super(true, "free", 0);
    }
}
