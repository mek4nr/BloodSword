package com.unidev.bloodsword.models.inventory;

public enum ESlot {
    ARMOUR,
    WEAPON
}