package com.unidev.bloodsword.models.inventory;

import android.media.Image;
import android.support.annotation.DrawableRes;

import com.unidev.bloodsword.models.Effect;
import com.unidev.bloodsword.models.actors.playableActors.PlayableActor;
import com.unidev.bloodsword.utils.Sound;

import java.util.ArrayList;
import java.util.Arrays;

public class UseableItem extends Item {

    protected Sound useSound;
    protected Effect[] effects;

    public UseableItem(String name, @DrawableRes int image, Sound useSound) {
        super(true, name, image);
        this.useSound = useSound;
    }

    public void use(PlayableActor user) {
        useSound.play();
        user.addEffects(effects);
    }
}