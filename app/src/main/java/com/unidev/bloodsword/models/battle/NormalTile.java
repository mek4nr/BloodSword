package com.unidev.bloodsword.models.battle;

import android.media.Image;

import com.unidev.bloodsword.models.actors.Actor;

/**
 * Created by jmuniere on 2017-03-15.
 */

public class NormalTile extends Tile {
    public NormalTile(int posX, int posY, Actor actor, Image image) {
        super(posX, posY, actor, image, true, false, false);
    }

    public NormalTile(int posX, int posY) {
        super(posX, posY, null, null, true, false, false);
    }
}
