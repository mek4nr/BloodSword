package com.unidev.bloodsword.models.battle;


public enum EMovementStatus {
    MONSTER_TILE,
    WALL_TILE,
    FAR_TILE,
    OK_TILE
}
