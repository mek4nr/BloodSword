package com.unidev.bloodsword.models.spells;

public class BlastingSpell extends Spell {
    public BlastingSpell(String name, int complexity, int maxTarget, int damageDice, int damageBonus, boolean bActiveArmour) {
        super(name, complexity, maxTarget, damageDice, damageBonus, bActiveArmour);
        this.description = "Spell{" +
                "name='" + name + '\'' +
                ", complexity=" + complexity +
                ", maxTarget=" + maxTarget +
                ", damageDice=" + damageDice +
                ", damageBonus=" + damageBonus +
                ", bActiveArmour=" + bActiveArmour +
                '}';
    }
}
