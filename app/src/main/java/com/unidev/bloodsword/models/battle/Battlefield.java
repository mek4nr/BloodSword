package com.unidev.bloodsword.models.battle;

import android.util.Log;
import android.widget.Toast;

import com.unidev.bloodsword.activities.BattlefieldActivity;
import com.unidev.bloodsword.models.EActionType;
import com.unidev.bloodsword.models.Game;
import com.unidev.bloodsword.models.actors.Actor;
import com.unidev.bloodsword.models.actors.ArcherActor;
import com.unidev.bloodsword.models.actors.HealActor;
import com.unidev.bloodsword.models.actors.SpellCasterActor;
import com.unidev.bloodsword.models.actors.enemyActors.EnemyActor;
import com.unidev.bloodsword.models.actors.playableActors.PlayableActor;
import com.unidev.bloodsword.models.spells.Spell;
import com.unidev.bloodsword.utils.Die;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class Battlefield implements Serializable{

    protected static final long serialVersionUID = 46L;
    private static BattlefieldActivity activity;
    private int sizeX;
    private int sizeY;
    private Tile[] tiles;
    private int turn;

    private List<EnemyActor> enemies = new ArrayList<>();
    private Actor target;
    private Spell spell;
    private EActionType action = EActionType.NO_ACTION;

    private int fleeChapter;
    private int defeatChapter;

    private ArrayList<Actor> actorsPlayingOrder = new ArrayList<>();

    public Battlefield(int sizeX, int sizeY, int fleePage, int defeatPage) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.fleeChapter = fleePage;
        this.defeatChapter = defeatPage;
        tiles = new Tile[sizeX*sizeY];

        for(int i=1; i <sizeX; i++){
            for(int j=0; j<sizeY; j++){
                this.setTile(i,j,new NormalTile(i,j,null, null));
            }
        }
    }

    /*
    AI ZONE
     */

    public void AISelectAction(){
        if(!(actorPlaying() instanceof EnemyActor))
            return;

        action = EActionType.FIGHT_MOVE;

        if(actorPlaying() instanceof ArcherActor && ((ArcherActor) actorPlaying()).canShoot(this)) {
            action = EActionType.SHOOT;
        }
    }

    public void AIProcessAction(){
        if(target != null){
            basicProcessAction();
        }
        else {
            postProcessAction();
        }
    }

    public void AISelectTarget(){
        if(!(actorPlaying() instanceof EnemyActor))
            return;

        target = nearestEnemy(this.actorPlaying());
    }

    /*
    Actions ZONE
     */

    public void basicProcessAction(Object ...objects){
        switch (action){
            case FIGHT_MOVE:
                fightMoveAction();
                break;
            case SHOOT:
                shootAction();
                break;
            case DEFENDING:
                actorPlaying().defend();
                toastDefending();
                break;
            case FLEE_MOVE:
                fleeMoveAction();
                break;
            case HEAL:
                ((HealActor)actorPlaying()).heal((HashMap<Actor,Integer>)objects[0]);
                break;
            case STORE_SPELL:
                ((SpellCasterActor)actorPlaying()).storeSpell((Spell)objects[0]);
                toastSpellStored((Spell)objects[0]);
                break;
            case CAST_SPELL:
                Spell spell = (this.spell == null) ? (Spell) objects[0] : this.spell;
                ArrayList<Actor> targets = new ArrayList<>();
                if(spell.needTarget()) {
                    targets.add(target);
                }
                else{
                    targets.addAll(getEnemies(actorPlaying()));
                }
                int damage = ((SpellCasterActor) actorPlaying()).useSpell(targets, spell, turn);
                if(damage == 0){
                    toastSpellCastFailed(spell);
                }
                else{
                    toastSpellCastHit(spell, damage);
                }
                break;
        }
        postProcessAction();
    }

    private void postProcessAction(){
        if(target != null && target.isDead()){
            toastKill();
        }

        if(Game.getInstance().isLose()){
            this.activity.goLoseActivity();
        }
        else if(isWin()){
            this.activity.goWinActivity();
        }
        else {
            removeDeadFromOrder();
            setAction(EActionType.NO_ACTION);
            nextPlayer();
        }
    }

    private void fightMoveAction(){
        Tile selectedTile = null;

        if(distance(target.getTile(), actorPlaying().getTile()) != 1){
            int x = target.getTile().getPosX();
            int y = target.getTile().getPosY();

            Tile[] arround = {getTile(x-1,y), getTile(x+1,y), getTile(x,y+1), getTile(x,y-1)};

            int dist = 0;

            for(Tile tile : arround){
                if(tile == null)
                    continue;
                int newDist = distance(actorPlaying().getTile(), tile);
                if(actorPlaying().canMove(tile) && (dist == 0 || dist > newDist)){
                    dist = newDist;
                    selectedTile = tile;
                }
            }
            if(distance(actorPlaying().getTile(), nearestEnemy(actorPlaying()).getTile()) == 1) {
                int damage = actorPlaying().fleeWound(nearestEnemies(actorPlaying()));
                if(damage > 0){
                    toastWoundDamage(damage);
                }
            }
        }
        else{
            selectedTile = actorPlaying().getTile();
        }

        if(!actorPlaying().isDead()) {
            int damage = actorPlaying().fightMove(target, selectedTile);
            if(damage == 0){
                toastMissed();
            }
            else{
                toastHit(damage);
            }
        }
        else{
            toastFleeKill();
        }
    }

    private void fleeMoveAction(){
        if(distance(actorPlaying().getTile(), nearestEnemy(actorPlaying()).getTile()) == 1) {
            int damage = actorPlaying().fleeWound(nearestEnemies(actorPlaying()));
            if(damage > 0){
                toastWoundDamage(damage);
            }
        }

        if(!actorPlaying().isDead()) {
            toastFlee();
            actorPlaying().fleeMove(null);
        }
        else{
            toastFleeKill();
        }
    }

    private void shootAction(){
        int damage = ((ArcherActor)actorPlaying()).shoot(target);
        if(damage == 0){
            toastMissed();
        }
        else{
            toastShoot(damage);
        }
    }

    /*
    Toast
     */

    private void toastMissed(){
        Toast.makeText(activity.getBaseContext(),String.format("%1$s missed %2$s", actorPlaying().getName(), target.getName()),
                Toast.LENGTH_SHORT).show();
    }

    private void toastHit(int damage){
        Toast.makeText(activity.getBaseContext(),String.format("%1$s hit %2$s for %3$s damages", actorPlaying().getName(), target.getName(), damage),
                Toast.LENGTH_SHORT).show();
    }

    private void toastShoot(int damage){
        Toast.makeText(activity.getBaseContext(),String.format("%1$s shoot %2$s for %3$s damages", actorPlaying().getName(), target.getName(), damage),
                Toast.LENGTH_SHORT).show();
    }

    private void toastSpellStored(Spell spell){
        Toast.makeText(activity.getBaseContext(),String.format("Spell : %1$s stored", spell.getName()),
                Toast.LENGTH_SHORT).show();
    }

    private void toastSpellCastHit(Spell spell, int damage){
        Toast.makeText(activity.getBaseContext(),String.format("%1$s launch spell : %2$s and hit %3$s for %4$s ",
                actorPlaying(),
                spell.getName(),
                (spell.needTarget())?target.getName():"all enemies",
                damage),
                Toast.LENGTH_SHORT).show();
    }

    private void toastSpellCastFailed(Spell spell){
        Toast.makeText(activity.getBaseContext(),String.format("%1$s launched %2$s and failed", spell.getName(), spell.getName()),
                Toast.LENGTH_SHORT).show();
    }

    private void toastWoundDamage(int damage){
        Toast.makeText(activity.getBaseContext(),String.format("%1$s hit %2$s for %3$s damages during his flee", actorPlaying().getName(), target.getName(), damage),
                Toast.LENGTH_SHORT).show();
    }

    private void toastFlee(){
        Toast.makeText(activity.getBaseContext(),String.format("%1$s is fleeing", actorPlaying().getName()),
                Toast.LENGTH_SHORT).show();
    }

    private void toastFleeKill(){
        Toast.makeText(activity.getBaseContext(),String.format("%1$s died while fleeing", actorPlaying().getName()),
                Toast.LENGTH_SHORT).show();
    }

    private void toastKill(){
        Toast.makeText(activity.getBaseContext(),String.format("%1$s killed %2$s", actorPlaying().getName(), target.getName()),
                Toast.LENGTH_SHORT).show();
    }

    private void toastDefending(){
        Toast.makeText(activity.getBaseContext(),String.format("%1$s is in defense mode", actorPlaying().getName()),
                Toast.LENGTH_SHORT).show();
    }

    /*
    Movement calcul
     */

    private boolean canMoveTo(Actor actor){
        int x = actor.getTile().getPosX();
        int y = actor.getTile().getPosY();

        Tile[] arround = {getTile(x-1,y), getTile(x+1,y), getTile(x,y+1), getTile(x,y-1)};

        for(Tile tile : arround){
            if(tile == null)
                continue;

            if(actorPlaying().canMove(tile)){
                return true;
            }
        }
        return false;
    }

    public ArrayList<Actor> nearestEnemies(Actor playingActor){
        ArrayList<Actor> allNearEnemy = new ArrayList<>();
        ArrayList<Actor> listEnemy = new ArrayList<>();

        if(playingActor instanceof EnemyActor) {
            listEnemy.addAll(Game.getInstance().getCharacters());
        }
        else {
            listEnemy.addAll(enemies);
        }

        int dist = 0;
        for(int i=0;i < listEnemy.size(); i++){
            Actor current_player = listEnemy.get(i);

            if(current_player.isDead() || !canMoveTo(current_player))
                continue;

            int newDist = distance(playingActor.getTile(), current_player.getTile());

            if(dist == 0){
                allNearEnemy.add(current_player);
                dist = newDist;
            }
            else if(newDist == dist){
                allNearEnemy.add(current_player);
            }
            else if(newDist < dist){
                allNearEnemy.clear();
                allNearEnemy.add(current_player);
                dist = newDist;
            }
        }

        return allNearEnemy;
    }

    public Actor nearestEnemy(Actor playingActor){
        ArrayList<Actor> allNearEnemy = nearestEnemies(playingActor);
        Actor nearest = null;

        if(allNearEnemy.size() > 1){
            int result = Die.roll(2) - 1;
            nearest = allNearEnemy.get((result/6));
        }
        else if(allNearEnemy.size() == 1){
            nearest = allNearEnemy.get(0);
        }

        return nearest;
    }

    public int distance(Tile tile1, Tile tile2){
        int dist = 0;
        dist += Math.abs(tile1.getPosX() - tile2.getPosX());
        dist += Math.abs(tile1.getPosY() - tile2.getPosY());

        return dist;
    }

    /*
    Actor list
     */

    private void nextPlayer(){
        target = null;
        spell = null;
        if(actorsPlayingOrder.size() != 0)
            actorsPlayingOrder.remove(0);

        if(actorsPlayingOrder.size() == 0)
            newTurn();
        else{
            this.activity.onPlayingActorChanged();
            this.activity.onTargetChanged();
            this.activity.handler.post(this.activity);
        }
    }

    private void newTurn(){
        if(Game.getInstance().isFleeing()){
            Game.getInstance().resetStatus();
            activity.goFleeActivity();
        }
        else{
            turn++;
            Game.getInstance().resetStatus();
            generateOrder();
            activity.onPlayingActorChanged();
            activity.onTargetChanged();
            activity.handler.post(activity);
        }
    }

    private void removeDeadFromOrder(){
        int i=0;
        while(i < actorsPlayingOrder.size()){
            if(actorsPlayingOrder.get(i).isDead()){
                actorsPlayingOrder.remove(i);
            }
            else{
                i++;
            }
        }
    }

    public void generateOrder(){
        actorsPlayingOrder.clear();
        actorsPlayingOrder.addAll(enemies);
        actorsPlayingOrder.addAll(Game.getInstance().getCharacters());

        removeDeadFromOrder();

        Collections.sort(actorsPlayingOrder, Actor.awerenessComparator);
    }

    /*
    Others
     */

    private boolean isWin(){
        for(EnemyActor actor : enemies){
            if(!actor.isDead())
                return false;
        }
        return true;
    }

    private boolean isEnemy(Actor actor){
        return((actorPlaying() instanceof PlayableActor && actor instanceof EnemyActor) ||
                (actorPlaying() instanceof EnemyActor && actor instanceof PlayableActor) );
    }

    private ArrayList<Actor> getEnemies(Actor actor){
        ArrayList<Actor> enemies = new ArrayList<>();
        if(actor instanceof PlayableActor){
            enemies.addAll(this.enemies);
        }
        else{
            enemies.addAll(Game.getInstance().getCharacters());
        }

        return enemies;
    }

    public boolean isPossibleTarget(Actor actor){
        return ((action != EActionType.FIGHT_MOVE || canMoveTo(actor)) && isEnemy(actor));

    }

    /*
    Getter & Setter
     */

    public Actor actorPlaying(){
        return (actorsPlayingOrder.size() > 0) ? actorsPlayingOrder.get(0) : null;
    }

    public Actor getTarget(){
        return target;
    }

    public void setTarget(Actor target) {
        this.target = target;
        this.activity.onTargetChanged();
    }

    private int getIndex(int x, int y){
        return y*sizeX+x;
    }

    public Tile getTile(int x, int y){
        return (x>=sizeX || y>=sizeY || x<0 || y<0) ? null : this.tiles[getIndex(x,y)];
    }

    public void setTile(int x, int y, Tile tile){
        tiles[getIndex(x, y)] = tile;
    }

    public List<Tile> getListTiles() {
        return Arrays.asList(this.tiles);
    }

    public int getSizeX() {
        return sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }

    public int getFleeChapter() {
        return fleeChapter;
    }

    public int getDefeatChapter() {
        return defeatChapter;
    }

    public void setEnemies(List<EnemyActor> enemies) {
        this.enemies = enemies;
    }

    public void addEnemy(EnemyActor enemy){
        this.enemies.add(enemy);
    }

    public EActionType getAction() {
        return action;
    }

    public void setAction(EActionType action) {
        if(action != this.action)
            this.target = null;
        this.action = action;
        this.activity.onActionChanged();
    }

    public BattlefieldActivity getActivity() {
        return activity;
    }

    public void setActivity(BattlefieldActivity activity) {
        this.activity = activity;
    }

    public void setSpell(Spell spell) {
        this.spell = spell;
    }

    public Spell getSpell() {
        return spell;
    }
}