package com.unidev.bloodsword.models.inventory;

import android.media.Image;
import android.support.annotation.DrawableRes;

public class MiscItem extends Item {
    public MiscItem(String name, @DrawableRes int image) {
        super(true, name, image);
    }
}