package com.unidev.bloodsword.models.book;

import com.unidev.bloodsword.models.actors.ECharacterType;

import java.io.Serializable;

public class Choice implements Serializable{

    // Attributs
    protected static final long serialVersionUID = 45L;
    private String text;
    private Page originChapter, destinationChapter, fleeDestinationChapter;
    private ECharacterType affectTo;

    // Constructeurs

    public Choice() {
    }

    public Choice(String text, Page originChapter, Page destinationChapter, Page fleeDestinationChapter, ECharacterType affectTo) {
        this.text = text;
        this.originChapter = originChapter;
        this.destinationChapter = destinationChapter;
        this.fleeDestinationChapter = fleeDestinationChapter;
        this.affectTo = affectTo;
    }

    public Choice(String text, Page originChapter, ECharacterType affectTo) {
        this.text = text;
        this.originChapter = originChapter;
        this.affectTo = affectTo;
    }

    /*
    Getter & Setter
     */

    public String getText() {
        return text;
    }

    public ECharacterType getAffectTo() {
        return affectTo;
    }

    public Page getPage() {
        return originChapter;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Page getOriginChapter() {
        return originChapter;
    }

    public void setOriginChapter(Page originChapter) {
        this.originChapter = originChapter;
    }

    public Page getDestinationChapter() {
        return destinationChapter;
    }

    public void setDestinationChapter(Page destinationChapter) {
        this.destinationChapter = destinationChapter;
    }

    public Page getFleeDestinationChapter() {
        return fleeDestinationChapter;
    }

    public void setFleeDestinationChapter(Page fleeDestinationChapter) {
        this.fleeDestinationChapter = fleeDestinationChapter;
    }

    public void setAffectTo(ECharacterType affectTo) {
        this.affectTo = affectTo;
    }
}