package com.unidev.bloodsword.models.book;

import android.graphics.Bitmap;
import android.media.Image;

import com.unidev.bloodsword.models.actors.EActorStat;
import com.unidev.bloodsword.models.actors.ECharacterType;

import java.io.Serializable;
import java.util.ArrayList;

public class Page implements Serializable{

    // Variables
    protected static final long serialVersionUID = 44L;
    private int chapter;
    private String text;
    private Boolean bBattle;
    private ArrayList<Choice> choices;
    private Bitmap image;
    private ECharacterType eCharacterType;
    private EActorStat checkedStat;
    private int checkSuccessDest, checkFailDest;

    // Constructeurs

    public Page() {
    }

    public Page(int chapter, String text, Boolean bBattle, ArrayList<Choice> choices, Bitmap image, ECharacterType eCharacterType, EActorStat checkedStat, int checkSuccessDest, int checkFailDest) {
        this.chapter = chapter;
        this.text = text;
        this.bBattle = bBattle;
        this.choices = choices;
        this.image = image;
        this.eCharacterType = eCharacterType;
        this.checkedStat = checkedStat;
        this.checkSuccessDest = checkSuccessDest;
        this.checkFailDest = checkFailDest;
    }

    public Page(int chapter, String text, Boolean bBattle, Bitmap image, ECharacterType eCharacterType, EActorStat checkedStat, int checkSuccessDest, int checkFailDest) {
        this.chapter = chapter;
        this.text = text;
        this.bBattle = bBattle;
        this.image = image;
        this.eCharacterType = eCharacterType;
        this.checkedStat = checkedStat;
        this.checkSuccessDest = checkSuccessDest;
        this.checkFailDest = checkFailDest;
    }

    // Accesseurs

    public int getChapter() {
        return chapter;
    }

    public String getText() {
        return text;
    }

    public Boolean getbBattle() {
        return bBattle;
    }

    public ArrayList<Choice> getChoices() {
        return choices;
    }

    public Bitmap getImage() {
        return image;
    }

    public ECharacterType geteCharacterType() {
        return eCharacterType;
    }

    public EActorStat getCheckedStat() {
        return checkedStat;
    }

    public int getCheckSuccessDest() {
        return checkSuccessDest;
    }

    public int getCheckFailDest() {
        return checkFailDest;
    }

    // Mutateurs

    public void setChapter(int chapter) {
        this.chapter = chapter;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setbBattle(Boolean bBattle) {
        this.bBattle = bBattle;
    }

    public void setChoices(ArrayList<Choice> choices) {
        this.choices = choices;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public void seteCharacterType(ECharacterType eCharacterType) {
        this.eCharacterType = eCharacterType;
    }

    public void setCheckedStat(EActorStat checkedStat) {
        this.checkedStat = checkedStat;
    }

    public void setCheckSuccessDest(int checkSuccessDest) {
        this.checkSuccessDest = checkSuccessDest;
    }

    public void setCheckFailDest(int checkFailDest) {
        this.checkFailDest = checkFailDest;
    }
}