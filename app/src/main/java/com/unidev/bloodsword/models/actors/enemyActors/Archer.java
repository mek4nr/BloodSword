package com.unidev.bloodsword.models.actors.enemyActors;

import com.unidev.bloodsword.models.actors.Actor;
import com.unidev.bloodsword.models.actors.ArcherActor;
import com.unidev.bloodsword.models.battle.Battlefield;
import com.unidev.bloodsword.models.inventory.Amunition;
import com.unidev.bloodsword.models.inventory.amorItems.ArmourItem;
import com.unidev.bloodsword.models.inventory.weaponItems.WeaponItem;
import com.unidev.bloodsword.utils.Sprite;

public class Archer extends EnemyActor implements ArcherActor {

    public Amunition amunition;
    public int diePenaltyShoot;

    public Archer(String name, Sprite sprite, int fightingProwess, int damageDice, int damageBonus, int psychicAbility, int awereness, int endurance, ArmourItem armour, WeaponItem weapon) {
        super(name, sprite, fightingProwess, damageDice, damageBonus, psychicAbility, awereness, endurance, armour, weapon);
    }

    @Override
    public int shoot(Actor target) {
        int tmp = this.damageDice;
        this.damageDice = 1;
        int damage = applyDamageTo(target,false,true);
        this.damageDice = tmp;
        return damage;
    }

    @Override
    public boolean canShoot(Battlefield battlefield) {
        Actor nearestEnnemy = battlefield.nearestEnemy(battlefield.actorPlaying());
        if(nearestEnnemy != null && battlefield.distance(battlefield.actorPlaying().getTile(), nearestEnnemy.getTile()) > 1 && hasAmmunition()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean hasAmmunition() {
        return true;
    }

}