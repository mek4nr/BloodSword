package com.unidev.bloodsword.models;

import android.content.Context;
import android.util.Log;

import com.unidev.bloodsword.managers.ChoseHeroManager;
import com.unidev.bloodsword.models.actors.ECharacterType;
import com.unidev.bloodsword.models.actors.playableActors.Enchanter;
import com.unidev.bloodsword.models.actors.playableActors.PlayableActor;
import com.unidev.bloodsword.models.actors.playableActors.Sage;
import com.unidev.bloodsword.models.actors.playableActors.Trickster;
import com.unidev.bloodsword.models.actors.playableActors.Warrior;
import com.unidev.bloodsword.models.battle.Battlefield;
import com.unidev.bloodsword.models.book.Page;
import com.unidev.bloodsword.widgets.ChoseHeroWidget;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Game implements Serializable
{
    private static final long serialVersionUID = 40L;
    private static final String saveExtension = ".save";
    private static Game game;
    private EDifficulty difficulty;
    private ArrayList<PlayableActor> characters;
    private String name;
    private Page currentPage;
    private Battlefield battlefield;

    private Game() {
        characters = new ArrayList<>();
    }

    /**
     * Ordering characters by a new order given
     * @param index1 first toon to trade
     * @param index2 second toon to trade
     */
    public void setOrder(int index1, int index2) {
        PlayableActor actor = characters.get(index1);
        characters.set(index1, characters.get(index2));
        characters.set(index2, actor);
    }


    public void newGame(List<ChoseHeroWidget> heroes, Context context){
        int cmp=0;
        characters.clear();

        for(ChoseHeroWidget hero : heroes){
            if(hero.activated)
                cmp++;
        }

        int rank = 0;
        switch(cmp) {
            case 1:
                rank = 8;
                break;
            case 2:
                rank = 4;
                break;
            case 3:
                rank = 3;
                break;
            case 4:
                rank = 2;
                break;
        }

        for(ChoseHeroWidget hero : heroes) {
            if (!hero.activated)
                continue;

            switch (hero.characterType) {
                case ENCHANTER:
                    characters.add(new Enchanter(hero.name, rank, context));
                    break;
                case WARRIOR:
                    characters.add(new Warrior(hero.name, rank, context));
                    break;
                case SAGE:
                    characters.add(new Sage(hero.name, rank, context));
                    break;
                case TRICKSTER:
                    characters.add(new Trickster(hero.name, rank, context));
                    break;
            }
        }
    }

    /**
     * Testing for generate a group in any activity.
     * Use only for testing purpose
     * @param cmp number of player in group
     */
    public void generateGroup(int cmp, Context context){
        List<ChoseHeroWidget> heroes = ChoseHeroManager.getAll();
        characters.clear();
        this.name = "generated";

        for(int i=0; i < cmp; i++){
            heroes.get(i).activated = true;
        }

        int rank = 0;
        switch(cmp){
            case 1 :
                rank = 8;
                break;
            case 2 :
                rank = 4;
                break;
            case 3 :
                rank = 3;
                break;
            case 4 :
                rank = 2;
                break;
        }

        for(ChoseHeroWidget hero : heroes) {
            if (!hero.activated)
                continue;

            switch (hero.characterType) {
                case ENCHANTER:
                    Enchanter enchanter = new Enchanter(hero.name, rank, context);
                    enchanter.setDamage(5,false);
                    enchanter.setAwereness(9999);
                    enchanter.setEndurance(9999);
                    characters.add(enchanter);
                    break;
                case TRICKSTER:
                    Trickster trickster = new Trickster(hero.name, rank, context);
//                    trickster.setDamage(5,true);
                    characters.add(trickster);
                    break;
                case WARRIOR:
                    Warrior warrior = new Warrior(hero.name, rank, context);
                    warrior.setDamage(5,false);
                    characters.add(warrior);
                    break;
                case SAGE:
                    Sage sage = new Sage(hero.name, rank, context);
                    characters.add(sage);
                    break;

            }
        }
    }

    /**
     * Return the index of the ECharacterType given
     * @param characterType the ECharacterType we are looking in the characters
     * @return index or -1
     */
    public int lookForCharacterType(ECharacterType characterType){
        int index = 0;
        for(PlayableActor playableActor : characters){
            if(characterType == playableActor.getCharacterType()){
                return index;
            }
            index++;
        }
        return -1;
    }

    public void endOfBattle(){
        this.battlefield = null;
    }

    /*
    Getter & setter
     */

    public Page getCurrentPage() {
        return currentPage;
    }

    public Battlefield getBattlefield() {
        return battlefield;
    }

    public void setCurrentPage(Page currentPage) {
        this.currentPage = currentPage;
    }

    public void setBattlefield(Battlefield battlefield) {
        this.battlefield = battlefield;
    }

    public PlayableActor getCharacter(int order) {

        if(order >= characters.size())
            return null;
        else
            return characters.get(order);
    }

    public PlayableActor getCharacter(ECharacterType type){
        for(PlayableActor playableActor : characters){
            if (playableActor.getCharacterType() == type)
                return playableActor;
        }
        return null;
    }

    public ArrayList<PlayableActor> getCharacters() {
        return characters;
    }

    public void setCharacters(ArrayList<PlayableActor> characters) {
        this.characters = characters;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EDifficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(EDifficulty difficulty) {
        this.difficulty = difficulty;
    }

    public ECharacterType[] getAllCharacterTypesInParty(){
        ECharacterType[] list = new ECharacterType[characters.size()];

        int index = 0;
        for(PlayableActor playableActor : characters){
            list[index] = playableActor.getCharacterType();
            index++;
        }

        return list;
    }

    public static Game getInstance() {

        if(game == null){
            game = new Game();
        }

        return game;
    }

    public boolean isLose(){
        for(PlayableActor actor : characters){
            if(!actor.isDead())
                return false;
        }
        return true;
    }

    public boolean isFleeing(){
        for(PlayableActor actor : characters){
            if(!actor.isFleeing() && !actor.isDead())
                return false;
        }
        return true;
    }

    public void resetStatus(){
        for(PlayableActor actor : this.characters){
            actor.resetStatus();
        }
    }

    /*
    Serializable
     */

    public boolean saveGame(Context context){
        try
        {
            FileOutputStream fos = context.openFileOutput(name+saveExtension, Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(this);
            os.close();
            fos.close();
            Log.d("plop", "saved");
            return true;
        }
        catch (final Exception e)
        {
            e.printStackTrace();
            Log.d("plop", e.toString());
            return false;
        }
    }

    private static List<File> getSaveFiles(File parentDir) {
        ArrayList<File> inFiles = new ArrayList<File>();
        File[] files = parentDir.listFiles();
        for (File file : files) {
            if(file.getName().endsWith(saveExtension)){
                inFiles.add(file);
            }
        }
        return inFiles;
    }

    public static ArrayList<String> getLoadGames(Context context){
        ArrayList<String> games = new ArrayList<>();

        for(File file : getSaveFiles(context.getFilesDir())){
            games.add(file.getName().substring(0,file.getName().lastIndexOf('.')));
        }
        return games;
    }

    public static void loadGame(Context context, String name){
        try
        {
            FileInputStream fileInputStream = context.openFileInput(name+saveExtension);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            game = (Game) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
        }
        catch (final FileNotFoundException e)
        {
            // NO FILE
        }
        catch (final IOException e)
        {
            //NOTHING TO LOAD
        }

        catch (final ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

}
