package com.unidev.bloodsword.models.actors.enemyActors;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.models.inventory.amorItems.ArmourItem;
import com.unidev.bloodsword.models.inventory.weaponItems.WeaponItem;
import com.unidev.bloodsword.utils.Sprite;

public class Barbarian extends EnemyActor {
    public Barbarian(int endurance) {
        super("Barbarian", new Sprite(R.drawable.barbarian), 8, 1, 2, 5, 7, endurance, new ArmourItem("leather", 1), new WeaponItem("sledgehammer", 1));
    }
}