package com.unidev.bloodsword.models.inventory.weaponItems;

import android.media.Image;
import android.support.annotation.DrawableRes;

import com.unidev.bloodsword.models.inventory.ESlot;
import com.unidev.bloodsword.models.inventory.EquipableItem;

public class WeaponItem extends EquipableItem {

    private int damage;

    public WeaponItem(String name, @DrawableRes int image, int damage) {
        super(name, image, ESlot.WEAPON);
        this.damage = damage;
    }

    public WeaponItem(String name, int damage) {
        super(name, ESlot.WEAPON);
        this.damage = damage;
    }
}