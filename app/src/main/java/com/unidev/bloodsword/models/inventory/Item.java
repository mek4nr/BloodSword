package com.unidev.bloodsword.models.inventory;

import android.media.Image;
import android.support.annotation.DrawableRes;

import java.io.Serializable;

public abstract class Item implements Serializable{

    protected static final long serialVersionUID = 43L;
    protected boolean bPlacedInInventory;
    protected String name;
    protected @DrawableRes int image;

    public Item(boolean bPlacedInInventory, String name, @DrawableRes int image) {
        this.bPlacedInInventory = bPlacedInInventory;
        this.name = name;
        this.image = image;
    }

    public String getName() {
        return name;
    }
}