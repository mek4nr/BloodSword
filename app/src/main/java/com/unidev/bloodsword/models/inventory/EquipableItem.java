package com.unidev.bloodsword.models.inventory;

import android.media.Image;
import android.support.annotation.DrawableRes;

public abstract class EquipableItem extends Item {

    protected final ESlot slot;

    public EquipableItem(String name, @DrawableRes int image, ESlot slot) {
        super(true, name, image);
        this.slot = slot;
    }

    public EquipableItem(String name, ESlot slot) {
        super(true, name, 0);
        this.slot = slot;
    }

    public ESlot getSlot() {
        return slot;
    }
}