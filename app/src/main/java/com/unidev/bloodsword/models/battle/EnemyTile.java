package com.unidev.bloodsword.models.battle;

import android.media.Image;

import com.unidev.bloodsword.models.actors.Actor;


public class EnemyTile extends Tile{
    public EnemyTile(int posX, int posY, Actor actor, Image image) {
        super(posX, posY, actor, image, true, true, false);
    }

    public EnemyTile(int posX, int posY) {
        super(posX, posY, null, null, true, true, false);
    }
}
