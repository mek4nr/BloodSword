package com.unidev.bloodsword.models.actors.enemyActors;

import com.unidev.bloodsword.models.actors.Actor;
import com.unidev.bloodsword.models.actors.SpellCasterActor;
import com.unidev.bloodsword.models.inventory.amorItems.ArmourItem;
import com.unidev.bloodsword.models.inventory.weaponItems.WeaponItem;
import com.unidev.bloodsword.models.spells.Spell;
import com.unidev.bloodsword.utils.Sprite;

import java.util.ArrayList;
import java.util.List;

public class SpellCaster extends EnemyActor implements SpellCasterActor {

    private ArrayList<Spell> spells = new ArrayList<>();

    public SpellCaster(String name, Sprite sprite, int fightingProwess, int damageDice, int damageBonus, int psychicAbility, int awereness, int endurance, ArmourItem armour, WeaponItem weapon) {
        super(name, sprite, fightingProwess, damageDice, damageBonus, psychicAbility, awereness, endurance, armour, weapon);
    }

    @Override
    public ArrayList<Spell> getSpells() {
        return spells;
    }

    @Override
    public ArrayList<Spell> getMemorySpells() {
        return spells;
    }

    @Override
    public boolean storeSpell(Spell spell) {
        return true;
    }

    @Override
    public int useSpell(List<Actor> target, Spell spell, int turn) {
        return 0;
    }

    @Override
    public boolean rollPsychicTest(Spell spell, int turn) {
        return false;
    }

    @Override
    public int launchSpell(List<Actor> targets, Spell spell) {
        return 0;
    }

    @Override
    public int canUseSpell(Spell spell) {
        return spells.indexOf(spell);
    }

    @Override
    public int canStoreSpell(Spell spell) {
        return spells.indexOf(spell);
    }
}
