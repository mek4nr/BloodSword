package com.unidev.bloodsword.models.actors.enemyActors;


public enum EEnemyType {
    BARBARIAN(1),
    ASSASSIN(2),
    MAGUS_VYL(3),
    MAN_IN_BLUE(4),
    QUEL(5);

    public int id;

    EEnemyType(int id) {
        this.id = id;
    }
}