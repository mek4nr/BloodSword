package com.unidev.bloodsword.models.inventory;

import android.util.Log;

import java.io.Serializable;

public class Inventory implements Serializable{

    protected static final long serialVersionUID = 42L;
    private int size = 10;
    private int sizeLeft = 10;
    private Item[] items;

    public Inventory() {
        items = new Item[size];
        clear();
    }

    public Inventory(int size) {
        this.size = size;
        this.sizeLeft = size;
        items = new Item[size];
        clear();
    }

    public final Item[] getItems() {
        return items;
    }

    /**
     * Remove all item from the inventory
     */
    public void clear(){
        for(int i = 0; i < size; i++){
            items[i] = getNullItem();
        }
        sizeLeft = size;
    }

    public NullItem getNullItem(){
        return new NullItem();
    }

    /**
     * Add storageItemItem to the inventory
     * @param item the kind of item to add
     * @param quantity the quantity to add
     * @return the quantity left (that cannot be added)
     */
    public int add(StorageItemItem item, int quantity){
        for (int i = 0; i < size; i++) {
            if(items[i] instanceof StorageItem){
                StorageItem storageItem = (StorageItem) items[i];
                if(storageItem.getItemAccepted().getClass() == item.getClass() && storageItem.getQuantityLeft() > 0){
                    int qte = Math.min(quantity, storageItem.getQuantityLeft());
                    storageItem.add(qte);
                    quantity-=qte;
                }
            }
        }

        return quantity;
    }

    public int canAdd(StorageItemItem item){
        int qte = 0;
        for (int i = 0; i < size; i++) {
            if(items[i] instanceof StorageItem){
                StorageItem storageItem = (StorageItem) items[i];
                if(storageItem.getItemAccepted().getClass() == item.getClass() && storageItem.getQuantityLeft() > 0){
                    qte += storageItem.getQuantityLeft();
                }
            }
        }
        return qte;
    }

    public boolean canAdd(){
        return sizeLeft > 0;
    }

    /**
     * Add Item to the inventory
     * @param item the kind of item to add
     * @return true if item added, else false
     */
    public boolean add(Item item){
        if(sizeLeft == 0){
            return false;
        }
        for(int i=0; i<size; i++){
            if(items[i] instanceof NullItem){
                items[i] = item;
                sizeLeft--;
                return true;
            }
        }
        return false;
    }

    /**
     * Trash item to the inventory
     * @param index the index of item to trash
     */
    public void trash(int index){
        items[index] = getNullItem();
        sizeLeft++;
    }

    /**
     * Trash item to the inventory
     * @param item the item
     * @return true if Item existed & be trashed, else false
     */
    public boolean trash(Item item){
        for(int i = 0; i < size; i++){
            if(items[i] == item){
                trash(i);
                return true;
            }
        }
        return false;
    }
}