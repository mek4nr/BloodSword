package com.unidev.bloodsword.models.actors;

import java.util.HashMap;

public interface HealActor {

    public boolean canHeal();
    public int maxLifePaid();
    public int maxLifeHeal();
    public void castHeal(int lifePaid);
    public void heal(HashMap<Actor,Integer> healed);

}
