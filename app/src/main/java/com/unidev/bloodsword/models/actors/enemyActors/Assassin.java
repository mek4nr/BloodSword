package com.unidev.bloodsword.models.actors.enemyActors;

import com.unidev.bloodsword.models.inventory.amorItems.ArmourItem;
import com.unidev.bloodsword.models.inventory.weaponItems.WeaponItem;
import com.unidev.bloodsword.utils.Sprite;

public class Assassin extends Archer {
    public Assassin(int fightingProwess, int damageDice, int damageBonus, int psychicAbility, int awereness, int endurance, ArmourItem armour, WeaponItem weapon) {
        super("Assassin", null, fightingProwess, damageDice, damageBonus, psychicAbility, awereness, endurance, armour, weapon);
    }
}