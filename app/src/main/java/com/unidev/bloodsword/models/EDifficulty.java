package com.unidev.bloodsword.models;

public enum EDifficulty {
    NORMAL,
    SURVIVAL
}
