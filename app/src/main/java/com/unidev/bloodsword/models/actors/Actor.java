package com.unidev.bloodsword.models.actors;

import android.content.res.Resources;
import android.support.annotation.DrawableRes;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.models.battle.Tile;
import com.unidev.bloodsword.models.Effect;
import com.unidev.bloodsword.models.inventory.amorItems.ArmourItem;
import com.unidev.bloodsword.models.inventory.weaponItems.WeaponItem;
import com.unidev.bloodsword.utils.Die;
import com.unidev.bloodsword.utils.Sprite;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public abstract class Actor implements Serializable{

    protected static final long serialVersionUID = 41L;
    protected String name;
    protected Sprite sprite;

    protected int fightingProwess;
    protected int damageDice;
    protected int damageBonus;
    protected int psychicAbility;
    protected int awareness;

    protected int baseEndurance;
    protected int endurance;

    protected ArmourItem armour;
    protected WeaponItem weapon;

    protected Tile tile;
    protected ArrayList<Effect> effects = new ArrayList<>();
    protected int diePenaltyFight = 0;
    protected boolean bFleeing = false;
    protected boolean bDefending = false;
    protected boolean bKnockOff = false;


    public Actor() {
    }

    public Actor(String name, Sprite sprite) {
        this.name = name;
        this.sprite = sprite;
    }

    public Actor(String name, Sprite sprite, int fightingProwess, int damageDice, int damageBonus, int psychicAbility, int awareness, int endurance, ArmourItem armour, WeaponItem weapon) {
        this.name = name;
        this.sprite = sprite;
        this.fightingProwess = fightingProwess;
        this.damageDice = damageDice;
        this.damageBonus = damageBonus;
        this.psychicAbility = psychicAbility;
        this.awareness = awareness;
        this.endurance = endurance;
        this.baseEndurance = endurance;
        this.armour = armour;
        this.weapon = weapon;
    }

    public Actor(Integer fightingProwess, Integer damageDice, Integer damageBonus, Integer psychicAbility, Integer awareness, Integer endurance) {
        this.fightingProwess = fightingProwess;
        this.damageDice = damageDice;
        this.damageBonus = damageBonus;
        this.psychicAbility = psychicAbility;
        this.awareness = awareness;
        this.endurance = endurance;
        this.baseEndurance = endurance;
    }

    public Actor(String name, Sprite sprite, Integer fightingProwess, Integer endurance, Integer psychicAbility, Integer awareness, Integer damageDice, Integer damageBonus) {
        this.name = name;
        this.sprite = sprite;
        this.fightingProwess = fightingProwess;
        this.endurance = endurance;
        this.baseEndurance = endurance;
        this.psychicAbility = psychicAbility;
        this.awareness = awareness;
        this.damageDice = damageDice;
        this.damageBonus = damageBonus;
    }

    /*
    Other
     */

    public void newGameStats(Actor actor){
        this.awareness = actor.awareness;
        this.endurance = actor.endurance;
        this.baseEndurance = endurance;
        this.fightingProwess = actor.fightingProwess;
        this.damageDice = actor.damageDice;
        this.damageBonus = actor.damageBonus;
        this.psychicAbility = actor.psychicAbility;
    }

    public void addEffects(Effect[] effects){
        this.effects.addAll(Arrays.asList(effects));
    }

    public void newTurn(){
        for(Effect effect : effects){
            effect.newTurn();
        }
    }

    /*
    Actions
     */

    public int applyDamageTo(Actor target, boolean bActiveBonus, boolean bArmourCount){
        int damage = Math.max(this.getDamage(target,bActiveBonus),0);
        return target.setDamage(damage,bArmourCount);
    }

    public int fleeWound(ArrayList<Actor> actors){
        int damage = 0;
        for(Actor actor : actors){
            if(this.awareness < actor.awareness){
                damage += actor.applyDamageTo(this,true,true);
            }
        }
        return damage;
    }

    public int fightMove(Actor target, Tile tile) {
        tile.setActor(this);
        return applyDamageTo(target,true,true);
    }

    public int setDamage(int damageReceive, boolean bArmourCount){
        int pureDamage = Math.max((damageReceive - ((bArmourCount) ? armour.getArmourPoint() : 0)),0);
        this.endurance -= pureDamage;
        this.endurance = Math.max(this.endurance,0);
        if(this.endurance == 0){
            this.getTile().setActor(null);
            this.setTile(null);
        }
        return pureDamage;
    }

    public int getDamage(Actor target, boolean bActiveBonus){
        int dieResult = Die.roll(((target.isDefending())? 3:2));

        if(dieResult <= this.fightingProwess){
            return Die.roll(this.damageDice) + ((bActiveBonus) ? this.damageBonus : 0);
        }
        else{
            return 0;
        }
    }

    public abstract boolean canMove(Tile tile);

    public void defend(){
        this.bDefending = true;
    }

    public void fleeMove(Tile tile){
        this.bFleeing = true;
    }

    public void resetStatus(){
        this.bDefending = false;
        this.bFleeing = false;
    }

    public void receiveHeal(int heal) {
        this.endurance += heal;
    }

    public void memoryBurn(){
        this.psychicAbility-=1;
    }

    public void memoryRelease(){
        this.psychicAbility+=1;
    }

    /*
    Utils
     */

    public static Comparator<Actor> awerenessComparator = new Comparator<Actor>() {

        public int compare(Actor actor1, Actor actor2) {
            //descending order
            return ((Integer)actor2.awareness).compareTo((Integer)actor1.awareness);
        }
    };

    /*
    Getter & setter
     */

    public int getBaseEndurance() {
        return baseEndurance;
    }

    public int getEndurance() {
        return endurance;
    }

    public boolean isDead(){
        return endurance <= 0;
    }

    public String getStatsToString(Resources ressource) {
        return ressource.getString(R.string.tv_stats_text,fightingProwess,damageDice, damageBonus, psychicAbility, awareness, endurance, baseEndurance);
    }

    public Tile getTile() {
        return tile;
    }

    public void setTile(Tile tile) {
        this.tile = tile;
    }

    public boolean isDefending() {
        return bDefending;
    }

    public boolean isFleeing() {
        return bFleeing;
    }

    public boolean isKnockOff() {
        return bKnockOff;
    }

    public int getFightTouchScore() {
        return 0;
    }

    private int getFleeTouchScore() {
        return 0;
    }

    public Boolean isAffectedByPsySpell() {
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setAwereness(int awereness) {
        this.awareness = awereness;
    }

    public int getAwareness() {
        return awareness;
    }

    public int getFightingProwess() {
        return fightingProwess;
    }

    public int getPsychicAbility() {
        return psychicAbility;
    }

    public void setBaseEndurance(int baseEndurance) {
        this.baseEndurance = baseEndurance;
    }

    public void setEndurance(int endurance) {
        this.endurance = endurance;
    }

    public static @DrawableRes int getImageResource() {
        return android.R.mipmap.sym_def_app_icon;
    }

}