package com.unidev.bloodsword.models.inventory;

import android.media.Image;
import android.support.annotation.DrawableRes;

public abstract class StorageItem extends Item {

    protected int quantity;
    protected int maxQuantity;
    protected int  initialQuantity;
    protected StorageItemItem itemAccepted;


    public StorageItem(String name, @DrawableRes int image, int quantity, int maxQuantity, int initialQuantity, StorageItemItem itemAccepted) {
        super(true, name, image);
        this.quantity = quantity;
        this.maxQuantity = maxQuantity;
        this.initialQuantity = initialQuantity;
        this.itemAccepted = itemAccepted;
    }

    public boolean add(int nbItems) {
        if(nbItems > getQuantityLeft()){
            return false;
        }
        else {
            quantity += nbItems;
            return true;
        }
    }

    public boolean use(int nbItems) {
        if(nbItems > getQuantity()){
            return false;
        }
        else {
            quantity -= nbItems;
            return true;
        }
    }

    public int getQuantity() {
        return quantity;
    }

    public int getQuantityLeft(){
        return maxQuantity-quantity;
    }

    public StorageItemItem getItemAccepted() {
        return itemAccepted;
    }
}