package com.unidev.bloodsword.models.actors;

import com.unidev.bloodsword.models.battle.Battlefield;

public interface ArcherActor {

    public int shoot(Actor target);

    public boolean canShoot(Battlefield battlefield);

    public boolean hasAmmunition();

}