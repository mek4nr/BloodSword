package com.unidev.bloodsword.models.actors.playableActors;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.util.Log;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.managers.SpellManager;
import com.unidev.bloodsword.models.actors.Actor;
import com.unidev.bloodsword.models.actors.ECharacterType;
import com.unidev.bloodsword.models.actors.SpellCasterActor;
import com.unidev.bloodsword.models.inventory.amorItems.ArmourItem;
import com.unidev.bloodsword.models.inventory.weaponItems.WeaponItem;
import com.unidev.bloodsword.models.spells.BlastingSpell;
import com.unidev.bloodsword.models.spells.Spell;
import com.unidev.bloodsword.utils.Die;
import com.unidev.bloodsword.utils.Sprite;

import java.util.ArrayList;
import java.util.List;

public class Enchanter extends PlayableActor implements SpellCasterActor {

    private int nbFails;
    private int turnFail=0;
    private Spell lastSpell;
    private ArrayList<Spell> spells = new ArrayList<>();
    private ArrayList<Spell> memorySpells = new ArrayList<>();


    public Enchanter(String name, int rank, Context context) {
        super(name, new Sprite(R.drawable.enchanter), ECharacterType.ENCHANTER, rank, context);
        spells.addAll(SpellManager.getEnchanterSpells());
    }


    public Enchanter(Integer fightingProwess, Integer damageDice, Integer damageBonus, Integer psychicAbility, Integer awereness, Integer endurance) {
        super(fightingProwess, damageDice, damageBonus, psychicAbility, awereness, endurance);
    }

    @Override
    public void newBattle() {
        nbFails = 0;
    }

    @Override
    public void newGameInventory(ECharacterType type) {
        this.weapon = new WeaponItem("sword", 0, 0);
        this.armour = new ArmourItem("silver armour", 0, 2);
    }

    @Override
    public ArrayList<Spell> getSpells() {
        return spells;
    }

    @Override
    public ArrayList<Spell> getMemorySpells() {
        return memorySpells;
    }

    @Override
    public boolean storeSpell(Spell spell) {
        int index = canStoreSpell(spell);
        if(index != -1){
            memorySpells.add(spell);
            this.memoryBurn();
            return true;
        }
        return false;
    }

    @Override
    public int useSpell(List<Actor> targets, Spell spell, int turn) {
        int index = canUseSpell(spell);
        if(index != -1 && rollPsychicTest(spell, turn)){
            int damage = launchSpell(targets, spell);
            this.memoryRelease();
            memorySpells.remove(index);
            return damage;
        }
        return 0;
    }

    @Override
    public boolean rollPsychicTest(Spell spell, int turn) {
        if(turnFail-1 == turn || spell != lastSpell){
            nbFails = 0;
            lastSpell = null;
            turnFail = 0;
        }

        int result = Die.roll(2)+spell.getComplexity()-nbFails;

        if(result < this.getPsychicAbility()){
            return true;
        }
        else {
            nbFails++;
            lastSpell=spell;
            return false;
        }
    }

    @Override
    public int launchSpell(List<Actor> targets, Spell spell) {
        if(spell instanceof BlastingSpell){
            int damage = Die.roll(spell.getDamageDice()) + spell.getDamageBonus();
            for(Actor target : targets){
                target.setDamage(damage,spell.isActiveArmour());
            }
            return damage;
        }
        return 0;
    }

    @Override
    public int canUseSpell(Spell spell) {
        return memorySpells.indexOf(spell);
    }

    @Override
    public int canStoreSpell(Spell spell) {
        int index = -1;
        if(psychicAbility > 1){
            index = spells.indexOf(spell);
        }
        return index;
    }

    public static @DrawableRes int getImageResource(){
        return R.drawable.portrait_enchanter;
    }

    public static String getDefaultName() {
        return "Marhen";
    }
}