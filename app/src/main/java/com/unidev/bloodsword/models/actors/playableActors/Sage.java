package com.unidev.bloodsword.models.actors.playableActors;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.util.Log;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.models.Game;
import com.unidev.bloodsword.models.actors.Actor;
import com.unidev.bloodsword.models.actors.ECharacterType;
import com.unidev.bloodsword.models.actors.HealActor;
import com.unidev.bloodsword.models.inventory.amorItems.ArmourItem;
import com.unidev.bloodsword.models.inventory.weaponItems.WeaponItem;
import com.unidev.bloodsword.utils.Die;
import com.unidev.bloodsword.utils.Sprite;

import java.util.HashMap;

public class Sage extends ArcherPlayableActor implements HealActor {

    private int healValue;

    public Sage(String name, int rank, Context context) {
        super(name, new Sprite(R.drawable.sage), ECharacterType.SAGE, rank, context);
    }

    public Sage(Integer fightingProwess, Integer damageDice, Integer damageBonus, Integer psychicAbility, Integer awereness, Integer endurance) {
        super(fightingProwess, damageDice, damageBonus, psychicAbility, awereness, endurance);
    }

    @Override
    public void newBattle() {
        healValue = 0;
    }

    @Override
    public void newGameInventory(ECharacterType type) {
        this.weapon = new WeaponItem("quarterstaff", 0, 0);
        this.armour = new ArmourItem("ringmail armour", 0, 2);
        super.newGameInventory(type);
    }

    public static @DrawableRes int getImageResource(){
        return R.drawable.portrait_sage;
    }

    public static String getDefaultName() {
        return "Sylhia";
    }

    @Override
    public boolean canHeal() {
        return endurance > 1;
    }

    @Override
    public int maxLifePaid() {
        return endurance-1;
    }

    @Override
    public int maxLifeHeal() {
        return healValue;
    }

    @Override
    public void castHeal(int lifePaid) {
        this.endurance -= lifePaid;
        this.healValue = lifePaid* Math.max(Die.roll(1)-2,0);
    }

    @Override
    public void heal(HashMap<Actor, Integer> healed) {
        for(PlayableActor actor : Game.getInstance().getCharacters()){
            actor.receiveHeal(healed.get(actor));
        }
    }
}