package com.unidev.bloodsword.models.actors.playableActors;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.util.Log;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.models.actors.Actor;
import com.unidev.bloodsword.models.actors.ArcherActor;
import com.unidev.bloodsword.models.actors.ECharacterType;
import com.unidev.bloodsword.models.battle.Battlefield;
import com.unidev.bloodsword.models.inventory.Arrow;
import com.unidev.bloodsword.models.inventory.Item;
import com.unidev.bloodsword.models.inventory.Quiver;
import com.unidev.bloodsword.models.inventory.StorageItemItem;
import com.unidev.bloodsword.models.inventory.amorItems.ArmourItem;
import com.unidev.bloodsword.models.inventory.weaponItems.WeaponItem;
import com.unidev.bloodsword.utils.Sprite;

public class Trickster extends ArcherPlayableActor {

    boolean bQuickThinking = false;

    public Trickster(String name, int rank, Context context) {
        super(name, new Sprite(R.drawable.trikster), ECharacterType.TRICKSTER, rank, context);
    }

    public Trickster(Integer fightingProwess, Integer damageDice, Integer damageBonus, Integer psychicAbility, Integer awereness, Integer endurance) {
        super(fightingProwess, damageDice, damageBonus, psychicAbility, awereness, endurance);
    }

    @Override
    public void newGameInventory(ECharacterType type) {
        this.weapon = new WeaponItem("sword", 0, 0);
        this.armour = new ArmourItem("studded leather armour", 0, 2);
        super.newGameInventory(type);
    }

    @Override
    public void newBattle() {
        bQuickThinking = false;
    }

    public static @DrawableRes int getImageResource(){
        return R.drawable.portrait_trickster;
    }

    public static String getDefaultName() {
        return "Illidan";
    }
}