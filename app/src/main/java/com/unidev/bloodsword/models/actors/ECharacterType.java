package com.unidev.bloodsword.models.actors;

public enum ECharacterType {
    WARRIOR(1),
    TRICKSTER(2),
    SAGE(3),
    ENCHANTER(4);

    public int id;

    ECharacterType(int id) {
        this.id = id;
    }
}