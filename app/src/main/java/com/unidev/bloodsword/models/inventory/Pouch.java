package com.unidev.bloodsword.models.inventory;

public class Pouch extends StorageItem {

    public Pouch(int quantity) {
        super("pouch", 0, quantity, 100, 0, new Gold());
    }
}