package com.unidev.bloodsword.models.spells;

import com.unidev.bloodsword.models.actors.Actor;

import java.util.ArrayList;

public class ResistablePsychicSpell extends PsychicSpell {

    protected int resistedDamageDice;

    public ResistablePsychicSpell(String name, int complexity, int damageDice, int damageBonus, boolean bActiveArmour, boolean resistable, int resistedDamageDice) {
        super(name, complexity, damageDice, damageBonus, bActiveArmour, resistable);
        this.resistedDamageDice = resistedDamageDice;
    }
}