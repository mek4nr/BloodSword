package com.unidev.bloodsword.models.battle;

import android.media.Image;
import android.util.Log;

import com.unidev.bloodsword.models.actors.Actor;

import java.io.Serializable;

public class Tile implements Serializable{

    protected static final long serialVersionUID = 47L;
    private int posX;
    private int posY;
    private Actor actor;
    private Image image;
    private boolean bMoveTile;
    private boolean bMonsterTile;
    private boolean bFleeTile;

    public Tile(int posX, int posY, Actor actor, Image image, boolean bMoveTile, boolean bMonsterTile, boolean bFleeTile) {
        this.posX = posX;
        this.posY = posY;
        if(actor != null) {
            this.actor = actor;
            this.actor.setTile(this);
        }
        this.image = image;
        this.bMoveTile = bMoveTile;
        this.bMonsterTile = bMonsterTile;
        this.bFleeTile = bFleeTile;
    }

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setActor(Actor actor) {
        if(this.actor != actor){
            this.actor = actor;
            if(this.actor != null) {
                if(this.actor.getTile() != null){
                    this.actor.getTile().setActor(null);
                }
                this.actor.setTile(this);
            }
        }
    }

    @Override
    public String toString() {
        return "Tile{" +
                "posX=" + posX +
                ", posY=" + posY +
                ", actor=" + actor +
                '}';
    }

    public Actor getActor() {
        return actor;
    }

    public boolean isEmpty(){
        return actor == null;
    }
}