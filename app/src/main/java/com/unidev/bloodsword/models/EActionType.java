package com.unidev.bloodsword.models;

public enum EActionType {
    FIGHT_MOVE,
    FLEE_MOVE,
    SHOOT,
    STORE_SPELL,
    CAST_SPELL,
    HEAL,
    DEFENDING,
    NO_ACTION
}
