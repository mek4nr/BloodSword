package com.unidev.bloodsword.models.actors.playableActors;

import android.content.Context;

import com.unidev.bloodsword.models.actors.Actor;
import com.unidev.bloodsword.models.actors.ArcherActor;
import com.unidev.bloodsword.models.actors.ECharacterType;
import com.unidev.bloodsword.models.battle.Battlefield;
import com.unidev.bloodsword.models.inventory.Arrow;
import com.unidev.bloodsword.models.inventory.Quiver;
import com.unidev.bloodsword.models.inventory.weaponItems.WeaponItem;
import com.unidev.bloodsword.utils.Sprite;

public abstract class ArcherPlayableActor extends PlayableActor implements ArcherActor {

    protected Quiver quiver;

    public ArcherPlayableActor(Integer fightingProwess, Integer damageDice, Integer damageBonus, Integer psychicAbility, Integer awereness, Integer endurance) {
        super(fightingProwess, damageDice, damageBonus, psychicAbility, awereness, endurance);
    }

    public ArcherPlayableActor(String name, Sprite sprite, ECharacterType characterType, int rank, Context context) {
        super(name, sprite, characterType, rank, context);
    }

    @Override
    public int shoot(Actor target) {
        int tmp = this.damageDice;
        this.damageDice = 1;
        int damage = applyDamageTo(target,false,true);
        this.damageDice = tmp;
        quiver.use(1);
        return damage;
    }

    @Override
    public boolean canShoot(Battlefield battlefield) {
        Actor nearestEnnemy = battlefield.nearestEnemy(battlefield.actorPlaying());

        if(nearestEnnemy != null && battlefield.distance(battlefield.actorPlaying().getTile(), nearestEnnemy.getTile()) > 1 && hasAmmunition()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean hasAmmunition() {
        return quiver.getQuantity() > 0;
    }

    @Override
    public void newGameInventory(ECharacterType type) {
        quiver = new Quiver(6, new Arrow());
        this.inventory.add(quiver);
        this.inventory.add(new WeaponItem("bow", 0, 0));
    }
}
