package com.unidev.bloodsword.models.inventory;

public class Quiver extends StorageItem {
    public Quiver(int quantity, StorageItemItem itemAccepted) {
        super("Quiver", 0, quantity, 9, 0, itemAccepted);
    }
}