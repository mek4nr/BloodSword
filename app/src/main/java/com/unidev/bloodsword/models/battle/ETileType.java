package com.unidev.bloodsword.models.battle;
/**
 * Created by guill on 26/03/2017.
 */
public enum ETileType {
    NORMAL_TILE(1),
    ENEMY_TILE(2),
    WALL_TILE(3),
    FLEE_TILE (4);
    public int id;
    ETileType(int id) {
        this.id = id;
    }


}