package com.unidev.bloodsword.models.inventory.amorItems;

import android.media.Image;
import android.support.annotation.DrawableRes;

import com.unidev.bloodsword.models.inventory.ESlot;
import com.unidev.bloodsword.models.inventory.EquipableItem;

public class ArmourItem extends EquipableItem {

    private int armourPoint;

    public ArmourItem(String name, int armourPoint) {
        super(name, ESlot.ARMOUR);
        this.armourPoint = armourPoint;
    }

    public ArmourItem(String name, @DrawableRes int image, int armourPoint) {
        super(name, image, ESlot.ARMOUR);
        this.armourPoint = armourPoint;
    }

    public int getArmourPoint() {
        return armourPoint;
    }
}