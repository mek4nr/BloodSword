package com.unidev.bloodsword.models.actors.enemyActors;

import com.unidev.bloodsword.models.inventory.amorItems.ArmourItem;
import com.unidev.bloodsword.models.inventory.weaponItems.WeaponItem;
import com.unidev.bloodsword.utils.Sprite;


public class MagusVyl extends SpellCaster {
    public MagusVyl(int fightingProwess, int damageDice, int damageBonus, int psychicAbility, int awereness, int endurance, ArmourItem armour, WeaponItem weapon) {
        super("Magus Vyl", null, fightingProwess, damageDice, damageBonus, psychicAbility, awereness, endurance, armour, weapon);
    }
}
