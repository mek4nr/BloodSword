package com.unidev.bloodsword.models.inventory;

import android.support.annotation.DrawableRes;

public abstract class StorageItemItem extends Item {
    public StorageItemItem(String name, @DrawableRes int image) {
        super(false, name, image);
    }
}