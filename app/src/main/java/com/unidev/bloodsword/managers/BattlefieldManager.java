package com.unidev.bloodsword.managers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.unidev.bloodsword.models.Game;
import com.unidev.bloodsword.models.actors.enemyActors.Assassin;
import com.unidev.bloodsword.models.actors.enemyActors.Barbarian;
import com.unidev.bloodsword.models.actors.enemyActors.EEnemyType;
import com.unidev.bloodsword.models.actors.enemyActors.EnemyActor;
import com.unidev.bloodsword.models.actors.enemyActors.MagusVyl;
import com.unidev.bloodsword.models.actors.enemyActors.ManInBlue;
import com.unidev.bloodsword.models.battle.Battlefield;
import com.unidev.bloodsword.models.battle.ETileType;
import com.unidev.bloodsword.models.battle.EnemyTile;
import com.unidev.bloodsword.models.battle.NormalTile;
import com.unidev.bloodsword.models.battle.Tile;
import com.unidev.bloodsword.models.battle.WallTile;
import com.unidev.bloodsword.models.book.Page;
import com.unidev.bloodsword.models.inventory.amorItems.ArmourItem;
import com.unidev.bloodsword.models.inventory.weaponItems.WeaponItem;
import com.unidev.bloodsword.utils.ConnexionBDD;

import java.util.ArrayList;
import java.util.Arrays;

public class BattlefieldManager {

    private final static String TILE_TABLE = "tiles";
    private final static String BATTLEFIELD_TABLE = "battlefields";
    private final static String ENEMY_TABLE = "enemies";

    private final static String BATTLEFIELD_PK = "originPage";
    private final static String ENEMY_PK = "name";
    private final static String TILE_PK = "battlefield";
    private final static String TILE_ENEMY_FK = "fk_enemy";
    private final static String TILE_ORDER_PLAYER = "order_character";


    public static Battlefield getBattlefield(Context context, Page page){
        SQLiteDatabase database = ConnexionBDD.getBDD(context);
        String queryBattlefield =
                String.format("SELECT * FROM %1$s WHERE %2$s = %3$s",
                        BATTLEFIELD_TABLE,
                        BATTLEFIELD_PK,
                        page.getChapter()
                );

        Cursor battlefield_cursor = database.rawQuery(queryBattlefield, null);
        battlefield_cursor.moveToFirst();
        Battlefield battlefield = new Battlefield(
                battlefield_cursor.getInt(battlefield_cursor.getColumnIndex("sizeX")),
                battlefield_cursor.getInt(battlefield_cursor.getColumnIndex("sizeY")),
                battlefield_cursor.getInt(battlefield_cursor.getColumnIndex("fleePage")),
                battlefield_cursor.getInt(battlefield_cursor.getColumnIndex("defeatPage")));

        String queryTiles =
                String.format("SELECT * " +
                                "FROM %1$s LEFT JOIN %2$s ON %2$s.%3$s = %1$s.%4$s " +
                                "WHERE %5$s = %6$s",
                        TILE_TABLE,
                        ENEMY_TABLE,
                        ENEMY_PK,
                        TILE_ENEMY_FK,
                        TILE_PK,
                        page.getChapter()
                );
        Cursor tiles_cursor = database.rawQuery(queryTiles, null);
        while (tiles_cursor.moveToNext()){
            Tile tile = generateTile(tiles_cursor);
            if(tile != null) {
                battlefield.setTile(
                        tile.getPosX(),
                        tile.getPosY(),
                        tile
                );
            }
            setActor(battlefield, tiles_cursor);
        }


        battlefield_cursor.close();
        tiles_cursor.close();

        return battlefield;
    }

    private static void setActor(Battlefield battlefield, Cursor cursor){
        int x = cursor.getInt(cursor.getColumnIndex("coordX")) -1;
        int y = cursor.getInt(cursor.getColumnIndex("coordY")) -1;
        EnemyActor enemy = generateEnemyActor(cursor);
        if(enemy != null) {
            battlefield.getTile(x, y).setActor(enemy);
            battlefield.addEnemy(enemy);
        }

        int order_character = cursor.getInt(cursor.getColumnIndex(TILE_ORDER_PLAYER)) - 1;
        if( order_character >= 0){
            battlefield.getTile(x,y).setActor(Game.getInstance().getCharacter(order_character));
        }
    }

    private static Tile generateTile(Cursor cursor){
        Tile tile = null;
        int x = cursor.getInt(cursor.getColumnIndex("coordX")) -1;
        int y = cursor.getInt(cursor.getColumnIndex("coordY")) -1;
        int typeTile = cursor.getInt(cursor.getColumnIndex("typeTile"));

        if(typeTile == ETileType.NORMAL_TILE.id || typeTile == ETileType.FLEE_TILE.id){
            tile = new NormalTile(x,y);
        }
        else if(typeTile == ETileType.WALL_TILE.id){
            tile = new WallTile(x,y);
        }
        else if(typeTile == ETileType.ENEMY_TILE.id){
            tile = new EnemyTile(x,y);
        }

        return tile;
    }

    private static EnemyActor generateEnemyActor(Cursor cursor){
        EnemyActor enemyActor = null;
        if(!cursor.isNull(cursor.getColumnIndex(ENEMY_PK))) {
            int enemyType = cursor.getInt(cursor.getColumnIndex(ENEMY_PK));

            if(enemyType == EEnemyType.BARBARIAN.id){
                enemyActor = new Barbarian(
                        cursor.getInt(cursor.getColumnIndex("endurance"))
                );
            }
            else if(enemyType == EEnemyType.ASSASSIN.id){
                enemyActor = new Assassin(
                        cursor.getInt(cursor.getColumnIndex("fightingProwess")),
                        cursor.getInt(cursor.getColumnIndex("damageDice")),
                        cursor.getInt(cursor.getColumnIndex("damageBonus")),
                        cursor.getInt(cursor.getColumnIndex("psychicAbility")),
                        cursor.getInt(cursor.getColumnIndex("awereness")),
                        cursor.getInt(cursor.getColumnIndex("endurance")),
                        new ArmourItem("", cursor.getInt(cursor.getColumnIndex("armour"))),
                        new WeaponItem("shuriken", cursor.getInt(cursor.getColumnIndex("weapon")))
                );
            }
            else if(enemyType == EEnemyType.MAGUS_VYL.id){
                enemyActor = new MagusVyl(
                        cursor.getInt(cursor.getColumnIndex("fightingProwess")),
                        cursor.getInt(cursor.getColumnIndex("damageDice")),
                        cursor.getInt(cursor.getColumnIndex("damageBonus")),
                        cursor.getInt(cursor.getColumnIndex("psychicAbility")),
                        cursor.getInt(cursor.getColumnIndex("awereness")),
                        cursor.getInt(cursor.getColumnIndex("endurance")),
                        new ArmourItem("", cursor.getInt(cursor.getColumnIndex("armour"))),
                        new WeaponItem("shuriken", cursor.getInt(cursor.getColumnIndex("weapon")))
                );
            }
            else if(enemyType == EEnemyType.MAN_IN_BLUE.id){
                enemyActor = new ManInBlue(
                        cursor.getInt(cursor.getColumnIndex("fightingProwess")),
                        cursor.getInt(cursor.getColumnIndex("damageDice")),
                        cursor.getInt(cursor.getColumnIndex("damageBonus")),
                        cursor.getInt(cursor.getColumnIndex("psychicAbility")),
                        cursor.getInt(cursor.getColumnIndex("awereness")),
                        cursor.getInt(cursor.getColumnIndex("endurance")),
                        new ArmourItem("", cursor.getInt(cursor.getColumnIndex("armour"))),
                        new WeaponItem("shuriken", cursor.getInt(cursor.getColumnIndex("weapon")))
                );
            }
            else if(enemyType == EEnemyType.QUEL.id){

            }
        }
        return enemyActor;
    }

    public static Battlefield getBattlefieldTest(Context context, Page page){
        Battlefield bf = new Battlefield(5,7,43,191);

        for(int i =0; i < 7; i++){
            bf.setTile(0,i,new WallTile(0,i));
            bf.setTile(4,i,new WallTile(4,i));
        }

        for(int i=1; i <= 3; i++){
            for(int j=0; j<7; j++){
                bf.setTile(i,j,new NormalTile(i,j,null, null));
            }
        }

        EnemyActor[] barbarians = {new Barbarian(100),new Barbarian(100),new Barbarian(100),new Barbarian(100)};

        bf.getTile(1,0).setActor(barbarians[0]);
        bf.getTile(3,3).setActor(barbarians[1]);
        bf.getTile(2,5).setActor(barbarians[2]);
        bf.getTile(3,5).setActor(barbarians[3]);
        bf.setEnemies(Arrays.asList(barbarians));

        Game g = Game.getInstance();
        g.generateGroup(4, context);
        g.setCurrentPage(PageManager.getPage(1, context));

        bf.getTile(1,1).setActor(g.getCharacter(0));
        bf.getTile(1,4).setActor(g.getCharacter(1));
        bf.getTile(2,0).setActor(g.getCharacter(2));
        bf.getTile(3,4).setActor(g.getCharacter(3));

        return bf;
    }
}
