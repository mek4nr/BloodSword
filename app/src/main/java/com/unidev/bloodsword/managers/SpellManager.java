package com.unidev.bloodsword.managers;

import com.unidev.bloodsword.models.spells.BlastingSpell;
import com.unidev.bloodsword.models.spells.Spell;

import java.util.ArrayList;


public class SpellManager {
    public static ArrayList<Spell> getEnchanterSpells(){
        ArrayList<Spell> spells = new ArrayList<>();
        spells.add(new BlastingSpell("Volcano",1,-1,1,0,true));
        spells.add(new BlastingSpell("White Fire",1,1,2,2,true));
        spells.add(new BlastingSpell("Swordthrus",2,1,3,3,true));
        spells.add(new BlastingSpell("Sheet Lightning",4,-1,2,2,true));
        spells.add(new BlastingSpell("Nemesis Bold",5,1,7,7,true));
        return spells;
    }
}
