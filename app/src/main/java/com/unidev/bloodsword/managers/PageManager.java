package com.unidev.bloodsword.managers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.models.actors.EActorStat;
import com.unidev.bloodsword.models.actors.ECharacterType;
import com.unidev.bloodsword.models.book.Choice;
import com.unidev.bloodsword.models.book.Page;
import com.unidev.bloodsword.utils.ConnexionBDD;

import java.util.ArrayList;

public class PageManager {

    // Attributs
    public static String PAGES_TABLE = "pages";
    public static String queryGetAll = "select * from " + PAGES_TABLE;

    // Méthodes

    /**
     * Get all the pages from the table
     */
    public static ArrayList<Page> getAll(Context ctx) {

        ArrayList<Page> result = new ArrayList<>();
        SQLiteDatabase db = ConnexionBDD.getBDD(ctx);

        Cursor c = db.rawQuery(queryGetAll, null);

        while(c.moveToNext()) {
            Page p = new Page();

            p.setChapter(c.getInt(0));

            p.setText(c.getString(1));

            if(c.getInt(2) == 0) {
                p.setbBattle(false);
            } else {
                p.setbBattle(true);
            }

            if(c.getString(3) == null) {
                p.setImage(null);
            }

            switch(c.getInt(4)) {
                case 1:
                    p.seteCharacterType(ECharacterType.WARRIOR);
                    break;
                case 2:
                    p.seteCharacterType(ECharacterType.TRICKSTER);
                    break;
                case 3:
                    p.seteCharacterType(ECharacterType.SAGE);
                    break;
                case 4:
                    p.seteCharacterType(ECharacterType.ENCHANTER);
                    break;
                default :
                    p.seteCharacterType(null);
            }

            switch(c.getInt(5)) {
                case 1:
                    p.setCheckedStat(EActorStat.FIGHTING_PROWESS);
                    break;
                case 2:
                    p.setCheckedStat(EActorStat.PSYCHIC_ABILITY);
                    break;
                case 3:
                    p.setCheckedStat(EActorStat.AWARENESS);
                    break;
                default:
                    p.setCheckedStat(null);
            }

            p.setCheckSuccessDest(c.getInt(6));

            p.setCheckFailDest(c.getInt(7));

            result.add(p);
        }

        c.close();
        ConnexionBDD.close();

        return result;
    }


    /**
     * Setting the chapter which is going to be displayed
     * @param chapterNb number of the chapter
     */
    public static Page getPage(int chapterNb, Context ctx){

        ArrayList<Page> pages = getAll(ctx);
        Page pageToFind = new Page();
        Boolean bPageFound = false;
        int cursor = 0;

        while(!bPageFound && cursor < pages.size()) {
            if(pages.get(cursor).getChapter() == chapterNb) {
                pageToFind = pages.get(cursor);
                bPageFound = true;
            } else {
                cursor++;
            }
        }

        return pageToFind;
    }
}
