package com.unidev.bloodsword.managers;

import com.unidev.bloodsword.models.actors.playableActors.PlayableActor;
import com.unidev.bloodsword.models.inventory.Inventory;
import com.unidev.bloodsword.models.inventory.Item;

import java.util.ArrayList;

public class InventoryManager {

    public static Item[] getInventory(PlayableActor character) {
        return character.getInventory().getItems();
    }
}
