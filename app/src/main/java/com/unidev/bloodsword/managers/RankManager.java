package com.unidev.bloodsword.managers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.unidev.bloodsword.models.actors.Actor;
import com.unidev.bloodsword.models.actors.ECharacterType;
import com.unidev.bloodsword.models.actors.playableActors.Enchanter;
import com.unidev.bloodsword.models.actors.playableActors.Sage;
import com.unidev.bloodsword.models.actors.playableActors.Trickster;
import com.unidev.bloodsword.models.actors.playableActors.Warrior;
import com.unidev.bloodsword.utils.ConnexionBDD;

import java.lang.reflect.Constructor;
import java.util.List;

import static com.unidev.bloodsword.managers.PageManager.queryGetAll;

public class RankManager {

    public final static String RANK_TABLE = "rank";
    public final static String RANK_LEVEL = "level";
    public final static String RANK_CTYPE = "characterClass";

    public static Actor getStatsFromRank(Context context, ECharacterType type, int rank){
        SQLiteDatabase database = ConnexionBDD.getBDD(context);
        String query = String.format("SELECT * FROM %1$s WHERE %2$s = %3$s AND %4$s = %5$s",RANK_TABLE, RANK_LEVEL,rank,RANK_CTYPE, type.id);

        Actor actor = null;

        Cursor c = database.rawQuery(query, null);
        c.moveToFirst();

        switch (type){
            case WARRIOR:
                actor = new Warrior(c.getInt(2),c.getInt(3),c.getInt(4),c.getInt(5),c.getInt(6),c.getInt(7));
                break;
            case TRICKSTER:
                actor = new Trickster(c.getInt(2),c.getInt(3),c.getInt(4),c.getInt(5),c.getInt(6),c.getInt(7));
                break;
            case ENCHANTER:
                actor = new Enchanter(c.getInt(2),c.getInt(3),c.getInt(4),c.getInt(5),c.getInt(6),c.getInt(7));
                break;
            case SAGE:
                actor = new Sage(c.getInt(2),c.getInt(3),c.getInt(4),c.getInt(5),c.getInt(6),c.getInt(7));
                break;
        }

        c.close();
        database.close();
        return actor;
    }
}
