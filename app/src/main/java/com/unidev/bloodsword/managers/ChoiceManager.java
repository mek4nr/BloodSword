package com.unidev.bloodsword.managers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.unidev.bloodsword.models.actors.ECharacterType;
import com.unidev.bloodsword.models.book.Choice;
import com.unidev.bloodsword.utils.ConnexionBDD;

import java.util.ArrayList;

public class ChoiceManager {

    // Attributs
    public static String CHOICES_TABLE = "choices";
    public static String queryGetAll = "select * from " + CHOICES_TABLE;

    // Méthodes

    /**
     * Get all the choices from the table
     */
    public static ArrayList<Choice> getAll(Context ctx) {
        ArrayList<Choice> result = new ArrayList<>();
        SQLiteDatabase db = ConnexionBDD.getBDD(ctx);

        Cursor myCursor = db.rawQuery(queryGetAll, null);

        while(myCursor.moveToNext()) {
            Choice c = new Choice();

            c.setOriginChapter(PageManager.getPage(myCursor.getInt(0), ctx));

            c.setDestinationChapter(PageManager.getPage(myCursor.getInt(1), ctx));

            c.setFleeDestinationChapter(PageManager.getPage(myCursor.getInt(2), ctx));

            c.setText(myCursor.getString(3));

            switch(myCursor.getInt(4)) {
                case 1:
                    c.setAffectTo(ECharacterType.WARRIOR);
                    break;
                case 2:
                    c.setAffectTo(ECharacterType.TRICKSTER);
                    break;
                case 3:
                    c.setAffectTo(ECharacterType.SAGE);
                    break;
                case 4:
                    c.setAffectTo(ECharacterType.ENCHANTER);
                    break;
                default:
                    c.setAffectTo(null);
            }

            result.add(c);
        }



        return result;
    }

}
