package com.unidev.bloodsword.managers;

import com.unidev.bloodsword.models.actors.ECharacterType;
import com.unidev.bloodsword.models.actors.playableActors.Enchanter;
import com.unidev.bloodsword.models.actors.playableActors.Sage;
import com.unidev.bloodsword.models.actors.playableActors.Trickster;
import com.unidev.bloodsword.models.actors.playableActors.Warrior;
import com.unidev.bloodsword.widgets.ChoseHeroWidget;

import java.util.ArrayList;
import java.util.List;

public class ChoseHeroManager {

    public static List<ChoseHeroWidget> getAll(){
        List<ChoseHeroWidget> retour = new ArrayList<>();

        retour.add(new ChoseHeroWidget(Enchanter.getDefaultName(), ECharacterType.ENCHANTER, Enchanter.getImageResource()));
        retour.add(new ChoseHeroWidget(Trickster.getDefaultName(), ECharacterType.TRICKSTER, Trickster.getImageResource()));
        retour.add(new ChoseHeroWidget(Sage.getDefaultName(), ECharacterType.SAGE, Sage.getImageResource()));
        retour.add(new ChoseHeroWidget(Warrior.getDefaultName(), ECharacterType.WARRIOR, Warrior.getImageResource()));
        return  retour;
    }
}
