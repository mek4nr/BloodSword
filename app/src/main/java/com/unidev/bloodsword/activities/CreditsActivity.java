package com.unidev.bloodsword.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.widgets.ButtonOldScroll;

import org.w3c.dom.Text;

public class CreditsActivity extends NoNavigationActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credits);

        TextView tv_app_name_credits = (TextView) findViewById(R.id.tv_app_name_credits);
        tv_app_name_credits.setTypeface(titleFont);

        TextView tv_credits_title = (TextView) findViewById(R.id.tv_credits_title);
        tv_credits_title.setTypeface(bodyFontBold);

        TextView tv_back_end_programmer = (TextView) findViewById(R.id.tv_back_end_programmer);
        tv_back_end_programmer.setTypeface(bodyFontBold);

        TextView tv_jb = (TextView) findViewById(R.id.tv_jb);
        tv_jb.setTypeface(bodyFontRegular);

        TextView tv_full_stack_programmer = (TextView) findViewById(R.id.tv_full_stack_programmer);
        tv_full_stack_programmer.setTypeface(bodyFontBold);

        TextView tv_guile = (TextView) findViewById(R.id.tv_guile);
        tv_guile.setTypeface(bodyFontRegular);

        TextView tv_visuals = (TextView) findViewById(R.id.tv_visuals);
        tv_visuals.setTypeface(bodyFontBold);

        TextView tv_nicolas = (TextView) findViewById(R.id.tv_nicolas);
        tv_nicolas.setTypeface(bodyFontRegular);

        ButtonOldScroll btnBackToMenu = (ButtonOldScroll) findViewById(R.id.btn_credits_to_menu);
        btnBackToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreditsActivity.this, MenuHomeActivity.class);
                startActivity(intent);
            }
        });
    }
}
