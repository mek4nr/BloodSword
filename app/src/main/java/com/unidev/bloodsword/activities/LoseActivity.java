package com.unidev.bloodsword.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.models.Game;

public class LoseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lose);
        // Save status of enemis here;
        Game.getInstance().endOfBattle();
    }
}
