package com.unidev.bloodsword.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.models.Game;

import java.io.File;
import java.util.ArrayList;

public class LoadGameActivity extends NoNavigationActivity {

    private ListView lv_saved_games;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_game);
        final ArrayList<String> games = Game.getLoadGames(this);

        lv_saved_games = (ListView) findViewById(R.id.lv_saved_games);
        lv_saved_games.setAdapter(new ArrayAdapter(this, android.R.layout.simple_list_item_1, games));
        lv_saved_games.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Game.loadGame(LoadGameActivity.this,games.get(position));

                if(Game.getInstance().getBattlefield() != null){
                    Intent intent = new Intent(LoadGameActivity.this, BattlefieldActivity.class);
                    startActivity(intent);
                }
                else{
                    Intent intent = new Intent(LoadGameActivity.this, BookActivity.class);
                    startActivity(intent);
                }
            }
        });

        Button btn_back_menu = (Button) findViewById(R.id.btn_back_menu);
        btn_back_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoadGameActivity.this, MenuHomeActivity.class);
                startActivity(intent);
            }
        });
    }
}
