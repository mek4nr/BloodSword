package com.unidev.bloodsword.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.models.EDifficulty;
import com.unidev.bloodsword.models.Game;
import com.unidev.bloodsword.widgets.ButtonOldScroll;
import com.unidev.bloodsword.widgets.ToggleButtonOldScroll;

public class NewGameActivity extends NoNavigationActivity {

    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_game);

        TextView tv_app_name = (TextView) findViewById(R.id.tv_app_name);
        tv_app_name.setTypeface(this.titleFont);

        TextView tv_new_game = (TextView) findViewById(R.id.tv_new_game);
        tv_new_game.setTypeface(bodyFontBold);

        TextView tv_enter_name = (TextView) findViewById(R.id.tv_enter_name);
        tv_enter_name.setTypeface(this.bodyFontRegular);

        EditText et_name = (EditText) findViewById(R.id.et_name);
        et_name.setTypeface(this.bodyFontRegular);
        et_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                name = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        TextView tv_difficulty = (TextView) findViewById(R.id.tv_difficulty);
        tv_difficulty.setTypeface(this.bodyFontRegular);

        ToggleButtonOldScroll tb_difficulty = (ToggleButtonOldScroll) findViewById(R.id.tb_difficulty);
        tb_difficulty.setTypeface(this.bodyFontBold);

        ButtonOldScroll btnChooseCompanions = (ButtonOldScroll) findViewById(R.id.btn_choose_companions);
        btnChooseCompanions.setTypeface(this.bodyFontBoldItalic);
        btnChooseCompanions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleButton tb_difficult = (ToggleButton) findViewById(R.id.tb_difficulty);
                EDifficulty difficulty = EDifficulty.NORMAL;

                if(name.equals("")) {
                    final AlertDialog.Builder noNameAlertBuilder = new AlertDialog.Builder(NewGameActivity.this);
                    noNameAlertBuilder.setTitle(R.string.ab_no_name_title);
                    noNameAlertBuilder.setMessage(R.string.ab_no_name_text);
                    final AlertDialog noNameAlert = noNameAlertBuilder.create();
                    noNameAlert.show();
                }
                else {
                    if (tb_difficult.isChecked()) {
                        difficulty = EDifficulty.SURVIVAL;
                    }

                    Game game = Game.getInstance();
                    game.setName(name);
                    game.setDifficulty(difficulty);

                    Intent intent = new Intent(NewGameActivity.this, ChooseCharactersActivity.class);
                    startActivity(intent);
                }
            }
        });

        ButtonOldScroll btnBackToMenu = (ButtonOldScroll) findViewById(R.id.btn_new_to_menu);
        btnBackToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewGameActivity.this, MenuHomeActivity.class);
                startActivity(intent);
            }
        });
    }
}
