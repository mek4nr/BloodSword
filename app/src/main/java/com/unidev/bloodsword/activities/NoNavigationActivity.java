package com.unidev.bloodsword.activities;

import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.unidev.bloodsword.utils.ConnexionBDD;

public abstract class NoNavigationActivity extends AppCompatActivity {

    public static float DP;
    public static int widthScreen, heightScreen;
    public static Typeface titleFont;
    public static Typeface bodyFontBold;
    public static Typeface bodyFontRegular;
    public static Typeface bodyFontItalic;
    public static Typeface bodyFontBoldItalic;
    public static AssetManager assetManager;
    public static String fontDir = "fonts/";

    public static View decorView;
    public static int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;


    public void immersiveMode() {
        final View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    // Surcharges
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!ConnexionBDD.checkDataBase(this)){
            ConnexionBDD.importBdd(this);
        }

        decorView = getWindow().getDecorView();
        if(assetManager == null){
            assetManager = this.getApplicationContext().getAssets();
            titleFont = Typeface.createFromAsset(assetManager, fontDir + "Barbarian.ttf");
            bodyFontBold = Typeface.createFromAsset(assetManager, fontDir + "Vollkorn-Bold.ttf");
            bodyFontBoldItalic = Typeface.createFromAsset(assetManager, fontDir + "Vollkorn-BoldItalic.ttf");
            bodyFontItalic = Typeface.createFromAsset(assetManager, fontDir + "Vollkorn-Italic.ttf");
            bodyFontRegular = Typeface.createFromAsset(assetManager, fontDir + "Vollkorn-Regular.ttf");
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        immersiveMode();
        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        immersiveMode();
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        decorView.setSystemUiVisibility(uiOptions);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initStatic();
    }

    public void initStatic(){
        if(DP == 0) {
            DisplayMetrics dm = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(dm);
            DP = dm.density;
            widthScreen = dm.widthPixels;
            heightScreen = dm.heightPixels;
        }
    }

    public static int dpToPx(int dp){
        return (int) Math.ceil(DP*dp);
    }
}
