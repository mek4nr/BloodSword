package com.unidev.bloodsword.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.adapters.BattlefieldAdapter;
import com.unidev.bloodsword.adapters.HealAdapter;
import com.unidev.bloodsword.adapters.SpellAdaptater;
import com.unidev.bloodsword.managers.BattlefieldManager;
import com.unidev.bloodsword.managers.PageManager;
import com.unidev.bloodsword.models.EActionType;
import com.unidev.bloodsword.models.Game;
import com.unidev.bloodsword.models.actors.ArcherActor;
import com.unidev.bloodsword.models.actors.ECharacterType;
import com.unidev.bloodsword.models.actors.HealActor;
import com.unidev.bloodsword.models.actors.SpellCasterActor;
import com.unidev.bloodsword.models.actors.enemyActors.EnemyActor;
import com.unidev.bloodsword.models.actors.playableActors.Enchanter;
import com.unidev.bloodsword.models.actors.playableActors.PlayableActor;
import com.unidev.bloodsword.models.battle.Battlefield;
import com.unidev.bloodsword.models.battle.Tile;
import com.unidev.bloodsword.models.spells.Spell;
import com.unidev.bloodsword.widgets.CustomViewPartyBar;


import java.util.HashMap;
import android.os.Handler;

public class BattlefieldActivity extends NoNavigationActivity implements Runnable{

    private GridView gv_battlefield;
    private LinearLayout ll_stats;
    private CustomViewPartyBar cv_partyBar;
    public Handler handler;
    private HashMap<EActionType, Button> actionsButtons = new HashMap<>();

    private BattlefieldAdapter battlefieldAdapter;
    private Battlefield battlefield;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_battlefield);

        handler = new Handler();
        Game game = Game.getInstance();

        if(game.getBattlefield() == null){
            game.setCurrentPage(PageManager.getPage(3, this));

            //DEBUG
            if(game.getCharacters().size() == 0)
                game.generateGroup(2,this);

            battlefield = BattlefieldManager.getBattlefield(this, game.getCurrentPage());
            battlefield.setActivity(this);
            game.setBattlefield(battlefield);
        }
        else{
            battlefield = game.getBattlefield();
            battlefield.setActivity(this);
        }

        //Bottom layout
        ll_stats = (LinearLayout) findViewById(R.id.ll_stats);

        // Heroes gridView
        cv_partyBar = (CustomViewPartyBar) findViewById(R.id.cv_partyBar);

        // BattleField grid
        gv_battlefield = (GridView) findViewById(R.id.gv_battlefield);
        gv_battlefield.setNumColumns(game.getBattlefield().getSizeX());
        battlefieldAdapter = new BattlefieldAdapter(this,R.layout.widget_tile,game.getBattlefield());
        gv_battlefield.setAdapter(battlefieldAdapter);

        gv_battlefield.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Tile tile = battlefield.getListTiles().get(position);
                if(tile.getActor() != null) {
                    if (battlefield.actorPlaying() instanceof PlayableActor && battlefield.getAction() != EActionType.NO_ACTION) {
                        if (tile.getActor() != null && tile.getActor() != battlefield.getTarget() && battlefield.isPossibleTarget(tile.getActor())) {
                            battlefield.setTarget(tile.getActor());
                        } else if (battlefield.getTarget() == tile.getActor() && battlefield.isPossibleTarget(tile.getActor())) {
                            battlefield.basicProcessAction();
                        }
                    } else {

                        battlefield.setTarget(tile.getActor());
                    }
                }
            }
        });

        initButton();
        onTargetChanged();
        onPlayingActorChanged();
        onActionChanged();
    }

    private void switchAction(EActionType action){
        if(action == battlefield.getAction()){
            battlefield.setAction(EActionType.NO_ACTION);
        }
        else{
            battlefield.setAction(action);
        }
    }

    private void initButton(){
        actionsButtons.put(EActionType.FIGHT_MOVE, (Button) findViewById(R.id.btn_fight_move));
        actionsButtons.put(EActionType.FLEE_MOVE, (Button) findViewById(R.id.btn_flee_move));
        actionsButtons.put(EActionType.DEFENDING, (Button) findViewById(R.id.btn_defending));
        actionsButtons.put(EActionType.SHOOT, (Button) findViewById(R.id.btn_shoot));
        actionsButtons.put(EActionType.CAST_SPELL, (Button) findViewById(R.id.btn_cast_spell));
        actionsButtons.put(EActionType.STORE_SPELL, (Button) findViewById(R.id.btn_store_spell));
        actionsButtons.put(EActionType.HEAL, (Button) findViewById(R.id.btn_cast_heal));

        actionsButtons.get(EActionType.FIGHT_MOVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchAction(EActionType.FIGHT_MOVE);
            }
        });
        actionsButtons.get(EActionType.FLEE_MOVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                battlefield.setAction(EActionType.FLEE_MOVE);
                battlefield.basicProcessAction();
            }
        });
        actionsButtons.get(EActionType.DEFENDING).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                battlefield.setAction(EActionType.DEFENDING);
                battlefield.basicProcessAction();
            }
        });
        actionsButtons.get(EActionType.SHOOT).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                battlefield.setAction(EActionType.SHOOT);
            }
        });
        actionsButtons.get(EActionType.HEAL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                battlefield.setAction(EActionType.HEAL);
                dialogBoxCastHeal();
            }
        });
        actionsButtons.get(EActionType.STORE_SPELL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                battlefield.setAction(EActionType.STORE_SPELL);
                dialogBoxStoreSpell();
            }
        });
        actionsButtons.get(EActionType.CAST_SPELL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchAction(EActionType.CAST_SPELL);
                dialogBoxCastSpell();
            }
        });
    }

    public void dialogBoxShareHeal(){
        final Dialog healDialog = new Dialog(BattlefieldActivity.this);
        healDialog.setCancelable(false);
        healDialog.setContentView(R.layout.dialog_share_heal);

        final ListView lv_heros_healed = (ListView) healDialog.findViewById(R.id.lv_heros_healed);
        Button btn_share_heal = (Button) healDialog.findViewById(R.id.btn_share_heal);
        final HealAdapter healAdapter = new HealAdapter(this, R.layout.widget_heal_slide, ((HealActor)battlefield.actorPlaying()).maxLifeHeal());
        TextView tv_max_heal = (TextView) healDialog.findViewById(R.id.tv_max_heal);
        tv_max_heal.setText("" + ((HealActor)battlefield.actorPlaying()).maxLifeHeal());

        lv_heros_healed.setAdapter(healAdapter);
        btn_share_heal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                battlefield.basicProcessAction(healAdapter.getHealed());
                onHeroesChanged();
                healDialog.dismiss();
            }
        });

        healDialog.show();
    }

    public void dialogBoxCastHeal(){
        final Dialog healDialog = new Dialog(BattlefieldActivity.this);
        healDialog.setCancelable(false);
        healDialog.setContentView(R.layout.dialog_cast_heal);

        final SeekBar sb_life_paid = (SeekBar) healDialog.findViewById(R.id.sb_life_paid);
        final TextView tv_life_paid = (TextView) healDialog.findViewById(R.id.tv_life_paid);

        final TextView tv_healer_name = (TextView) healDialog.findViewById(R.id.tv_healer_name);
        final ImageView iv_healer = (ImageView) healDialog.findViewById(R.id.iv_healer);

        tv_healer_name.setText(battlefield.actorPlaying().getName());
        iv_healer.setImageResource(battlefield.actorPlaying().getImageResource());

        tv_life_paid.setText("" + sb_life_paid.getProgress());

        sb_life_paid.setMax(((HealActor)battlefield.actorPlaying()).maxLifePaid()-1);
        sb_life_paid.setProgress(0);
        sb_life_paid.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tv_life_paid.setText("" + (progress+1));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        Button btn_cast_heal = (Button) healDialog.findViewById(R.id.btn_cast_heal);
        btn_cast_heal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HealActor)battlefield.actorPlaying()).castHeal(sb_life_paid.getProgress()+1);
                onHeroesChanged();
                dialogBoxShareHeal();
                healDialog.dismiss();
            }
        });

        Button btn_cancel_heal = (Button) healDialog.findViewById(R.id.btn_cancel_heal);
        btn_cancel_heal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                healDialog.dismiss();
            }
        });
        healDialog.show();
    }

    public void dialogBoxStoreSpell(){
        final Dialog storeDialog = new Dialog(BattlefieldActivity.this);
        storeDialog.setCancelable(false);
        storeDialog.setContentView(R.layout.dialog_store_spell);

        ListView lv_spell = (ListView) storeDialog.findViewById(R.id.lv_spell);
        final SpellAdaptater spellAdaptater = new SpellAdaptater(this,R.layout.widget_spell,((Enchanter)Game.getInstance().getCharacter(ECharacterType.ENCHANTER)).getSpells());
        lv_spell.setAdapter(spellAdaptater);
        lv_spell.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                battlefield.basicProcessAction(spellAdaptater.getItem(position));
                storeDialog.dismiss();
            }
        });

        Button btn_cancel = (Button) storeDialog.findViewById(R.id.btn_cancel_spell);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storeDialog.dismiss();
            }
        });

        storeDialog.show();
    }

    public void dialogBoxCastSpell(){
        final Dialog castSpell = new Dialog(BattlefieldActivity.this);
        castSpell.setCancelable(false);
        castSpell.setContentView(R.layout.dialog_store_spell);

        ListView lv_spell = (ListView) castSpell.findViewById(R.id.lv_spell);
        final SpellAdaptater spellAdaptater = new SpellAdaptater(this,R.layout.widget_spell,((Enchanter)Game.getInstance().getCharacter(ECharacterType.ENCHANTER)).getMemorySpells());
        lv_spell.setAdapter(spellAdaptater);
        lv_spell.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Spell spell = spellAdaptater.getItem(position);
                if(!spell.needTarget()){
                    battlefield.basicProcessAction(spell);
                }
                else{
                    battlefield.setSpell(spell);
                    onActionChanged();
                }
                castSpell.dismiss();
            }
        });

        Button btn_cancel = (Button) castSpell.findViewById(R.id.btn_cancel_spell);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                castSpell.dismiss();
            }
        });

        castSpell.show();
    }

    public void onActionChanged(){
        battlefieldAdapter.updateAction();
        gv_battlefield.setAdapter(battlefieldAdapter);
    }

    public void onTargetChanged(){
        battlefieldAdapter.updateTarget();
        if(battlefield.getTarget() != null) {
            generateStatsLayout();
            ll_stats.invalidate();
        }
        gv_battlefield.setAdapter(battlefieldAdapter);
    }

    public void onHeroesChanged(){
        cv_partyBar.onHeroesChanged();
    }

    public void onPlayingActorChanged(){
        battlefieldAdapter.updatePlayingActor();
        onHeroesChanged();
        generateActionLayout();
        resetStatsLayout();
    }

    private void generateActionLayout(){
        actionsButtons.get(EActionType.FIGHT_MOVE).setEnabled(false);
        actionsButtons.get(EActionType.FLEE_MOVE).setEnabled(false);
        actionsButtons.get(EActionType.DEFENDING).setEnabled(false);
        actionsButtons.get(EActionType.SHOOT).setEnabled(false);
        actionsButtons.get(EActionType.CAST_SPELL).setEnabled(false);
        actionsButtons.get(EActionType.STORE_SPELL).setEnabled(false);
        actionsButtons.get(EActionType.HEAL).setEnabled(false);

        if(battlefield.actorPlaying() instanceof PlayableActor){
            actionsButtons.get(EActionType.FIGHT_MOVE).setEnabled(true);
            actionsButtons.get(EActionType.FLEE_MOVE).setEnabled(true);
            actionsButtons.get(EActionType.DEFENDING).setEnabled(true);

            if((battlefield.actorPlaying() instanceof ArcherActor) && ((ArcherActor) battlefield.actorPlaying()).canShoot(battlefield)) {
                actionsButtons.get(EActionType.SHOOT).setEnabled(true);
            }
            if(battlefield.actorPlaying() instanceof SpellCasterActor && -1 != ((SpellCasterActor)battlefield.actorPlaying()).canStoreSpell(((SpellCasterActor)battlefield.actorPlaying()).getSpells().get(0))) {
                actionsButtons.get(EActionType.STORE_SPELL).setEnabled(true);
            }
            if(battlefield.actorPlaying() instanceof SpellCasterActor && ((SpellCasterActor)battlefield.actorPlaying()).getMemorySpells().size() > 0) {
                actionsButtons.get(EActionType.CAST_SPELL).setEnabled(true);
            }
            if((battlefield.actorPlaying() instanceof HealActor)) {
                actionsButtons.get(EActionType.HEAL).setEnabled(true);
            }
        }
    }

    private void resetStatsLayout(){
        ImageView iv_playing_actor = (ImageView) findViewById(R.id.iv_playing_actor);
        TextView tv_playing_actor_name = (TextView) findViewById(R.id.tv_playing_actor_name);
        TextView tv_stats = (TextView) findViewById(R.id.tv_stats);
        iv_playing_actor.setImageResource(0);
        tv_playing_actor_name.setText("");
        tv_stats.setText("");
    }

    private void generateStatsLayout(){
        ImageView iv_playing_actor = (ImageView) findViewById(R.id.iv_playing_actor);
        TextView tv_playing_actor_name = (TextView) findViewById(R.id.tv_playing_actor_name);
        iv_playing_actor.setImageResource(battlefield.getTarget().getImageResource());
        tv_playing_actor_name.setText(battlefield.getTarget().getName());

        TextView tv_stats = (TextView) findViewById(R.id.tv_stats);
        tv_stats.setText(battlefield.getTarget().getStatsToString(getResources()));
    }

    @Override
    public void run() {
        if (battlefield.actorPlaying() instanceof EnemyActor) {
            battlefield.AISelectAction();
            battlefield.AISelectTarget();
            battlefield.AIProcessAction();
            resetStatsLayout();
        }
    }

    public void goLoseActivity(){
        Intent intent = new Intent(this, LoseActivity.class);
        startActivity(intent);
    }

    public void goWinActivity(){
        Intent intent = new Intent(this, WinActivity.class);
        startActivity(intent);
    }

    public void goFleeActivity(){
        Intent intent = new Intent(this, FleeActivity.class);
        startActivity(intent);
    }
}