package com.unidev.bloodsword.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.widgets.ButtonOldScroll;

public class RulesActivity extends NoNavigationActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rules);

        TextView tv_app_name_rules = (TextView) findViewById(R.id.tv_app_name_rules);
        tv_app_name_rules.setTypeface(titleFont);

        TextView tv_rules_title = (TextView) findViewById(R.id.tv_rules_title);
        tv_rules_title.setTypeface(bodyFontBold);

        ButtonOldScroll btnBackToMenu = (ButtonOldScroll) findViewById(R.id.btn_rules_to_menu);
        btnBackToMenu.setTypeface(bodyFontBoldItalic);
        btnBackToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RulesActivity.this, MenuHomeActivity.class);
                startActivity(intent);
            }
        });
    }
}
