package com.unidev.bloodsword.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.utils.ConnexionBDD;

public class TitleActivity extends NoNavigationActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title);

        Animation anim = AnimationUtils.loadAnimation(this, R.anim.fade_title);

        TextView tv_title_series = (TextView) findViewById(R.id.tv_title_series);
        tv_title_series.setTypeface(this.titleFont);
        TextView tv_title_opus = (TextView) findViewById(R.id.tv_title_opus);
        tv_title_opus.setTypeface(this.bodyFontBold);
        TextView tv_title_book = (TextView) findViewById(R.id.tv_title_book);
        tv_title_book.setTypeface(this.bodyFontBold);
        TextView tv_touch_screen = (TextView) findViewById(R.id.tv_touch_screen);
        tv_touch_screen.setTypeface(this.bodyFontBoldItalic);

        tv_touch_screen.startAnimation(anim);


    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Intent intent = new Intent(this, MenuHomeActivity.class);
        startActivity(intent);
        return true;
    }
}
