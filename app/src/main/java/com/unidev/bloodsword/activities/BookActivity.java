package com.unidev.bloodsword.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.adapters.PagesAdapter;
import com.unidev.bloodsword.managers.ChoiceManager;
import com.unidev.bloodsword.managers.PageManager;
import com.unidev.bloodsword.models.Game;
import com.unidev.bloodsword.models.actors.EActorStat;
import com.unidev.bloodsword.models.actors.playableActors.PlayableActor;
import com.unidev.bloodsword.models.book.Choice;
import com.unidev.bloodsword.models.book.Page;
import com.unidev.bloodsword.widgets.ButtonOldScroll;
import com.unidev.bloodsword.widgets.CustomViewPartyBar;

public class BookActivity extends NoNavigationActivity {

    // Attributs
    private PagesAdapter pagesAdapter;
    private LinearLayout ll_BookMainLayout, ll_PageInnerLayout;
    private CustomViewPartyBar cv_partyBar;
    private ScrollView sv_PageMainLayout;
    private TextView tv_chapterTitle;
    private WebView wv_chapterText;
    private Page currentPage;
    private Game game;
    private Context ctx;

    // Surcharges
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        ctx = this;

        game = Game.getInstance();

        init();

        currentPage = Game.getInstance().getCurrentPage();

        updatePage();
    }

    // Méthodes

    /**
     * Initializing the page of the book
     */
    private void init() {
        // Création du gestionnaire de pages
        pagesAdapter = new PagesAdapter(ctx, R.layout.activity_book);

        // Récupération dans le XML de la vue du titre
        TextView tv_app_name_book = (TextView) findViewById(R.id.tv_app_name_book);
        tv_app_name_book.setTypeface(titleFont);

        // Récupération dans le XML du layout principal de l'activité
        ll_BookMainLayout = (LinearLayout) findViewById(R.id.ll_book_main_layout);

        // Création et ajout au layout principal de la barre de menu du groupe
        cv_partyBar = (CustomViewPartyBar) findViewById(R.id.cv_partyBar);

        // Création et ajout au layout principal du layout déroulant
        sv_PageMainLayout = (ScrollView) findViewById(R.id.sv_PageMainLayout);

        // Création et ajout du layout intérieur au layout déroulant
        ll_PageInnerLayout = (LinearLayout) findViewById(R.id.ll_PageInnerLayout);
    }

    /**
     * Updating the content of the displayed page
     */
    private void updatePage() {

        // Suppression de tous les enfants du layout intérieur
        ll_PageInnerLayout.removeAllViews();

        // Création et ajout du titre du chapitre au layout intérieur
        tv_chapterTitle = new TextView(ctx);
        tv_chapterTitle.setGravity(Gravity.CENTER);
        tv_chapterTitle.setTextSize(24);
        tv_chapterTitle.setTypeface(Typeface.DEFAULT_BOLD);
        tv_chapterTitle.setText("Chapter " + currentPage.getChapter());
        tv_chapterTitle.setTypeface(bodyFontBold);
        ll_PageInnerLayout.addView(tv_chapterTitle);

        // Création et ajout du texte du chapitre au layout intérieur
        wv_chapterText = new WebView(ctx);
        wv_chapterText.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        wv_chapterText.loadData("<![CDATA[<html><head></head>"
                        + "<body style=\"text-align:justify;\">"
                        + currentPage.getText()
                        + "</body></html>",
                "text/html", "utf-8");
        wv_chapterText.setBackgroundColor(Color.TRANSPARENT);
        ll_PageInnerLayout.addView(wv_chapterText);

        // Création et affichage des boutons de choix au layout intérieur
        if(currentPage.getbBattle()) {
            // Page avec combat
            ButtonOldScroll btn = new ButtonOldScroll(ctx, null, true);
            btn.setText(R.string.btn_book_battle);
            ll_PageInnerLayout.addView(btn, new LinearLayout.LayoutParams(ViewGroup.LayoutParams
                    .MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            setButtonActionBattle(btn);
        } else if (currentPage.getCheckedStat() != null) {
            // Page avec test de caractéristique
            ButtonOldScroll btn_try = new ButtonOldScroll(ctx, null, true);
            btn_try.setText(R.string.btn_try);
            ll_PageInnerLayout.addView(btn_try, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            setButtonTestCharacteristic(btn_try, currentPage.getCheckedStat(), Game.getInstance().getCharacter(currentPage.geteCharacterType()));

            addChoicesButtons();
        } else {
            // Page avec choix simples
            addChoicesButtons();
        }
    }

    /**
     * Setting the OnClickListener on the selected button according to the actual Choice
     * @param selectedButton set the button on which
     * @param buttonChoice take the information from the selected choice
     */
    private void setButtonActionNewPage(ButtonOldScroll selectedButton, final Choice buttonChoice) {
        selectedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                game.setCurrentPage(PageManager.getPage(buttonChoice.getDestinationChapter().getChapter(), getBaseContext()));

                currentPage = Game.getInstance().getCurrentPage();

                updatePage();
            }
        });
    }

    private void setButtonActionBattle(ButtonOldScroll selectedButton){
        selectedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BookActivity.this, BattlefieldActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setButtonTestCharacteristic(ButtonOldScroll selectedButton, final EActorStat stat, final PlayableActor character) {
        selectedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(character.testStat(stat, 2)) {
                    game.setCurrentPage(PageManager.getPage(currentPage.getCheckSuccessDest(), getBaseContext()));

                    currentPage = Game.getInstance().getCurrentPage();

                    updatePage();
                } else {
                    game.setCurrentPage(PageManager.getPage(currentPage.getCheckFailDest(), getBaseContext()));

                    currentPage = Game.getInstance().getCurrentPage();

                    updatePage();
                }
            }
        });
    }

    private void addChoicesButtons() {

        for(Choice c : ChoiceManager.getAll(ctx)) {
            if(c.getOriginChapter().getChapter() == currentPage.getChapter()) {
                if(c.getAffectTo() == null || Game.getInstance().lookForCharacterType(c.getAffectTo()) != -1) {
                    ButtonOldScroll btn = new ButtonOldScroll(ctx, null, true);
                    btn.setText(c.getText());
                    ll_PageInnerLayout.addView(btn, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    setButtonActionNewPage(btn, c);
                }
            }


        }
    }
}
