package com.unidev.bloodsword.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.widgets.ButtonOldScroll;

public class MenuHomeActivity extends NoNavigationActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_home);

        TextView tv_app_name_menu = (TextView) findViewById(R.id.tv_app_name_menu);
        tv_app_name_menu.setTypeface(this.titleFont);

        ButtonOldScroll btnNewGame = (ButtonOldScroll) findViewById(R.id.btn_new_game);
        ButtonOldScroll btnLoadGame = (ButtonOldScroll) findViewById(R.id.btn_load_game);
        ButtonOldScroll btnRules = (ButtonOldScroll) findViewById(R.id.btn_rules);
        ButtonOldScroll btnGlossary = (ButtonOldScroll) findViewById(R.id.btn_glossary);
        ButtonOldScroll btnCredits = (ButtonOldScroll) findViewById(R.id.btn_credits);
        ButtonOldScroll btnQuit = (ButtonOldScroll) findViewById(R.id.btn_quit);

        btnNewGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuHomeActivity.this, NewGameActivity.class);
                startActivity(intent);
            }
        });

        btnLoadGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuHomeActivity.this, LoadGameActivity.class);
                startActivity(intent);
            }
        });

        btnRules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuHomeActivity.this, RulesActivity.class);
                startActivity(intent);
            }
        });

        btnGlossary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuHomeActivity.this, GlossaryActivity.class);
                startActivity(intent);
            }
        });

        btnCredits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuHomeActivity.this, CreditsActivity.class);
                startActivity(intent);
            }
        });

        btnQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                moveTaskToBack(true);
            }
        });
    }
}
