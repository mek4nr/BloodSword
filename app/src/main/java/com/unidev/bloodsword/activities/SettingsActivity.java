package com.unidev.bloodsword.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.unidev.bloodsword.R;

public class SettingsActivity extends NoNavigationActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }
}
