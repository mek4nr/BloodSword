package com.unidev.bloodsword.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.adapters.InventoryAdapter;
import com.unidev.bloodsword.adapters.PlayableActorAdapter;
import com.unidev.bloodsword.adapters.ShareAdapter;
import com.unidev.bloodsword.models.Game;
import com.unidev.bloodsword.models.actors.playableActors.PlayableActor;
import com.unidev.bloodsword.models.inventory.EquipableItem;
import com.unidev.bloodsword.models.inventory.Item;
import com.unidev.bloodsword.models.inventory.NullItem;
import com.unidev.bloodsword.models.inventory.StorageItem;
import com.unidev.bloodsword.models.inventory.UseableItem;
import com.unidev.bloodsword.widgets.ButtonOldScroll;

import java.util.ArrayList;

public class InventoryActivity extends NoNavigationActivity {

    private GridView gv_heroes;
    private GridView gv_inventory;
    private LinearLayout ll_menu_btn;
    private int selectedHero;
    private Button btn_use, btn_trash, btn_equip, btn_give;
    private int selectedItem;
    private ArrayList<PlayableActor> heroTrades;

    private PlayableActorAdapter playableActorAdapter;
    private InventoryAdapter inventoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory);

        Game game = Game.getInstance();
        selectedHero = getIntent().getIntExtra("selected_hero",0);

        // Heroes gridView
        gv_heroes = (GridView) findViewById(R.id.gv_heroes);
        playableActorAdapter = new PlayableActorAdapter(this,R.layout.widget_hero, game.getCharacters());
        gv_heroes.setAdapter(playableActorAdapter);
        gv_heroes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedHero=position;
                onSelectedHeroChanged();
            }
        });

        //Inventory gridView

        ll_menu_btn = (LinearLayout) findViewById(R.id.ll_menu_btn);

        gv_inventory = (GridView) findViewById(R.id.gv_hero_inventory);
        gv_inventory.setNumColumns(2);
        onSelectedHeroChanged();
        gv_inventory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedItem = position;
                onSelectItemChanged();
            }
        });

        ButtonOldScroll btn_back = (ButtonOldScroll) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = null;
                if(Game.getInstance().getBattlefield() == null){
                    intent = new Intent(InventoryActivity.this, BookActivity.class);
                }
                else{
                    intent = new Intent(InventoryActivity.this, BattlefieldActivity.class);
                }
                startActivity(intent);
            }
        });

        //Inventory buttons
        btn_use = (Button) findViewById(R.id.btn_use);
        btn_use.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UseableItem useableItem = (UseableItem) inventoryAdapter.getItem(selectedItem);
                Game.getInstance().getCharacter(selectedHero).use(useableItem);
                Game.getInstance().getCharacter(selectedHero).getInventory().trash(useableItem);
            }
        });

        btn_equip = (Button) findViewById(R.id.btn_equip);
        btn_equip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EquipableItem equipableItem = (EquipableItem) inventoryAdapter.getItem(selectedItem);
                Game.getInstance().getCharacter(selectedHero).getInventory().trash(selectedItem);
                Game.getInstance().getCharacter(selectedHero).equip(equipableItem);
                selectedItem = -1;
                onSelectedHeroChanged();
                onSelectItemChanged();
            }
        });

        btn_give = (Button) findViewById(R.id.btn_give);
        btn_give.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedItem != -1)
                    dialogBoxGiveItem();
            }
        });

        btn_trash = (Button) findViewById(R.id.btn_trash);
        btn_trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedItem != -1){
                    Game.getInstance().getCharacter(selectedHero).getInventory().trash(selectedItem);
                }
            }
        });
        onSelectItemChanged();

    }

    private void onSelectItemChanged(){
        if(selectedItem != -1 && !(inventoryAdapter.getItem(selectedItem) instanceof NullItem)){
            heroTrades = heroCouldAdd();
            if(heroTrades.size() > 0)
                btn_give.setEnabled(true);

            btn_trash.setEnabled(true);

            if(inventoryAdapter.getItem(selectedItem) instanceof UseableItem){
                btn_use.setEnabled(true);
            }

            if(inventoryAdapter.getItem(selectedItem) instanceof EquipableItem){
                btn_equip.setEnabled(true);
            }
        }
        else{
            btn_give.setEnabled(false);
            btn_use.setEnabled(false);
            btn_equip.setEnabled(false);
            btn_trash.setEnabled(false);
        }
        ll_menu_btn.invalidate();
    }

    private void onSelectedHeroChanged(){
        generateStatsLayout();
        selectedItem = -1;
        inventoryAdapter = new InventoryAdapter(this,R.layout.widget_item, Game.getInstance().getCharacter(selectedHero).getInventory().getItems());
        gv_inventory.setAdapter(inventoryAdapter);
    }

    private void generateStatsLayout(){
        ImageView iv_playing_actor = (ImageView) findViewById(R.id.iv_playing_actor);
        TextView tv_playing_actor_name = (TextView) findViewById(R.id.tv_playing_actor_name);
        iv_playing_actor.setImageResource(Game.getInstance().getCharacter(selectedHero).getImageResource());
        tv_playing_actor_name.setText(Game.getInstance().getCharacter(selectedHero).getName());

        TextView tv_stats = (TextView) findViewById(R.id.tv_stats);
        tv_stats.setText(Game.getInstance().getCharacter(selectedHero).getStatsToString(getResources()));
    }

    private ArrayList<PlayableActor> heroCouldAdd(){
        PlayableActor trader = playableActorAdapter.getItem(selectedHero);
        ArrayList<PlayableActor> characters = new ArrayList<>();
        Item item = inventoryAdapter.getItem(selectedItem);
        for (PlayableActor actor :  Game.getInstance().getCharacters()) {
            if(actor != trader){
                if(item instanceof StorageItem){
                    if(actor.getInventory().canAdd(((StorageItem) item).getItemAccepted()) > 0) {
                        characters.add(actor);
                    }
                }
                else if (actor.getInventory().canAdd()) {
                    characters.add(actor);
                }
            }
        }
        return characters;
    }

    public void dialogBoxGiveItem(){
        final Dialog giveItemDialog = new Dialog(this);
        giveItemDialog.setCancelable(true);
        giveItemDialog.setContentView(R.layout.dialog_give_item);

        final ShareAdapter shareAdapter = new ShareAdapter(this,R.layout.widget_trade_choice, inventoryAdapter.getItem(selectedItem), playableActorAdapter.getItem(selectedHero));

        ListView ll_heroes = (ListView) giveItemDialog.findViewById(R.id.lv_heroes);
        ll_heroes.setAdapter(shareAdapter);
        final EditText et_quantity = (EditText) giveItemDialog.findViewById(R.id.et_quantity);
        LinearLayout ll_quantity = (LinearLayout) giveItemDialog.findViewById(R.id.ll_quantity);
        final boolean bNoNeedQuantity = !(inventoryAdapter.getItem(selectedItem) instanceof StorageItem);

        if(bNoNeedQuantity)
        {
            ((ViewGroup) ll_quantity.getParent()).removeView(ll_quantity);
        }
        else {
            et_quantity.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    int quantity = Integer.parseInt(s.toString());
                    if (inventoryAdapter.getItem(selectedItem) instanceof StorageItem) {
                        StorageItem storageItem = (StorageItem) inventoryAdapter.getItem(selectedItem);
                        if (quantity > storageItem.getQuantity()) {
                            et_quantity.setText("" + storageItem.getQuantity());
                        }
                    }
                }
            });
        }
        ll_heroes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                PlayableActor receiver = shareAdapter.getItem(position);
                PlayableActor trader = playableActorAdapter.getItem(selectedHero);
                Item item = inventoryAdapter.getItem(selectedItem);
                if(!bNoNeedQuantity && et_quantity.getText().toString().equals("")) {
                    //// TODO: 25/03/2017 make toast
                }
                else
                {
                    if(bNoNeedQuantity){
                        receiver.getInventory().add(item);
                        trader.getInventory().trash(item);
                    }
                    else{
                        int quantity = Integer.parseInt(et_quantity.getText().toString());

                        StorageItem storageItem = (StorageItem) item;
                        quantity -= receiver.getInventory().add(storageItem.getItemAccepted(), quantity);
                        storageItem.use(quantity);
                    }
                    selectedItem=-1;
                    onSelectItemChanged();
                    onSelectedHeroChanged();
                    giveItemDialog.dismiss();
                }

            }
        });

        Button btn_cancel = (Button) giveItemDialog.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                giveItemDialog.dismiss();
            }
        });
        giveItemDialog.show();
    }
}
