package com.unidev.bloodsword.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.widgets.ButtonOldScroll;

public class GlossaryActivity extends NoNavigationActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_glossary);

        TextView tv_app_name_glossary = (TextView) findViewById(R.id.tv_app_name_glossary);
        tv_app_name_glossary.setTypeface(titleFont);

        TextView tv_glossary_title = (TextView) findViewById(R.id.tv_glossary_title);
        tv_glossary_title.setTypeface(bodyFontBold);

        ButtonOldScroll btnBackToMenu = (ButtonOldScroll) findViewById(R.id.btn_glossary_to_menu);
        btnBackToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GlossaryActivity.this, MenuHomeActivity.class);
                startActivity(intent);
            }
        });
    }
}
