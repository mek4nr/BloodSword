package com.unidev.bloodsword.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.models.Game;

public class WinActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_win);
        // Save status of battle here
        Game.getInstance().endOfBattle();
    }
}
