package com.unidev.bloodsword.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.adapters.ChoseHeroAdapter;
import com.unidev.bloodsword.managers.ChoseHeroManager;
import com.unidev.bloodsword.managers.PageManager;
import com.unidev.bloodsword.models.Game;
import com.unidev.bloodsword.models.actors.playableActors.PlayableActor;
import com.unidev.bloodsword.widgets.ButtonOldScroll;
import com.unidev.bloodsword.widgets.ChoseHeroWidget;

import java.util.ArrayList;
import java.util.List;

public class ChooseCharactersActivity extends NoNavigationActivity {

    List<ChoseHeroWidget> heroes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_characters);

        TextView tv_app_name_choose_hero = (TextView) findViewById(R.id.tv_app_name_choose_hero);
        tv_app_name_choose_hero.setTypeface(titleFont);
        TextView tv_choose_hero = (TextView) findViewById(R.id.tv_choose_hero);
        tv_choose_hero.setTypeface(bodyFontBold);
        TextView tv_choose_hero_comment = (TextView) findViewById(R.id.tv_choose_hero_comment);
        tv_choose_hero_comment.setTypeface(bodyFontItalic);

        GridView gv = (GridView) findViewById(R.id.gv_heroes);
        heroes = ChoseHeroManager.getAll();

        gv.setAdapter(new ChoseHeroAdapter(this, R.layout.widget_chose_hero, heroes));

        ButtonOldScroll btnLaunchGame = (ButtonOldScroll) findViewById(R.id.btn_launch_game);

        btnLaunchGame.setTypeface(this.bodyFontBoldItalic);
        btnLaunchGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<PlayableActor> characters = new ArrayList<>();

                boolean b1Hero=false;

                for(ChoseHeroWidget hero : heroes){
                    if(hero.activated) {
                        b1Hero = true;
                        break;
                    }
                }

                if(!b1Hero){
                    final AlertDialog.Builder noHeroAlertBuilder = new AlertDialog.Builder(ChooseCharactersActivity.this);
                    noHeroAlertBuilder.setTitle(R.string.ab_no_hero_title);
                    noHeroAlertBuilder.setMessage(R.string.ab_no_hero_text);
                    final AlertDialog noHeroAlert = noHeroAlertBuilder.create();
                    noHeroAlert.show();
                }
                else{
                    Game game = Game.getInstance();
                    game.newGame(heroes, ChooseCharactersActivity.this);
                    game.setCurrentPage(PageManager.getPage(1, getBaseContext()));
                    Intent intent = new Intent(ChooseCharactersActivity.this, BookActivity.class);
                    startActivity(intent);
                }
            }
        });

        ButtonOldScroll btnBackToMenu = (ButtonOldScroll) findViewById(R.id.btn_choose_to_menu);
        btnBackToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChooseCharactersActivity.this, MenuHomeActivity.class);
                startActivity(intent);
            }
        });
    }
}