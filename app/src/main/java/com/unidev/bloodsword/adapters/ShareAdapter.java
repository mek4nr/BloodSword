package com.unidev.bloodsword.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.models.Game;
import com.unidev.bloodsword.models.actors.Actor;
import com.unidev.bloodsword.models.actors.playableActors.PlayableActor;
import com.unidev.bloodsword.models.inventory.Item;
import com.unidev.bloodsword.models.inventory.StorageItem;
import com.unidev.bloodsword.models.inventory.StorageItemItem;

import java.util.ArrayList;
import java.util.HashMap;

public class ShareAdapter extends ArrayAdapter<PlayableActor> {

    private @LayoutRes int layout;
    private Item item;

    public ShareAdapter(@NonNull Context context, @LayoutRes int resource, Item item, PlayableActor trader) {
        super(context, 0);
        ArrayList<PlayableActor> characters = new ArrayList<>();
        Log.d("plop", "item" + item.toString());
        for (PlayableActor actor :  Game.getInstance().getCharacters()) {
            if(actor != trader){
                if(item instanceof StorageItem){
                    if(actor.getInventory().canAdd(((StorageItem) item).getItemAccepted()) > 0) {
                        characters.add(actor);
                    }
                }
                else if (actor.getInventory().canAdd()) {
                    characters.add(actor);
                }
            }
        }
        this.item = item;

        Log.d("plop", "" + characters.size());
        addAll(characters);
        layout = resource;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final PlayableActor current_actor = getItem(position);


        if(convertView == null){
            LayoutInflater inflator = LayoutInflater.from(getContext());
            convertView = inflator.inflate(layout,null);
        }

        ImageView iv_hero = (ImageView) convertView.findViewById(R.id.iv_hero);
        TextView tv_hero_name = (TextView) convertView.findViewById(R.id.tv_hero_name);
        TextView tv_max_quantity = (TextView) convertView.findViewById(R.id.tv_max_quantity);

        iv_hero.setImageResource(current_actor.getImageResource());
        tv_hero_name.setText(current_actor.getName());
        if(item instanceof StorageItem){
            tv_max_quantity.setText("" + ((StorageItem) item).getQuantity());
        }
        else{
            tv_max_quantity.setText("");
        }

        return convertView;
    }
}
