package com.unidev.bloodsword.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.activities.InventoryActivity;
import com.unidev.bloodsword.activities.NoNavigationActivity;
import com.unidev.bloodsword.models.Game;
import com.unidev.bloodsword.models.actors.playableActors.PlayableActor;
import com.unidev.bloodsword.models.inventory.Item;
import com.unidev.bloodsword.models.inventory.NullItem;

import java.util.List;


public class PlayableActorAdapter extends ArrayAdapter<PlayableActor> {
    @LayoutRes private int layout;
    private List<PlayableActor> heroes;
    private int selectedHeros = -1;
    public GridView gv_heroes;

    public PlayableActorAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<PlayableActor> objects) {
        super(context, 0, objects);
        this.layout = resource;
        this.heroes = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        PlayableActor hero = getItem(position);

        if(convertView == null){
            LayoutInflater inflator = LayoutInflater.from(getContext());
            convertView = inflator.inflate(layout,null);
        }

        ImageView ivHero = (ImageView) convertView.findViewById(R.id.iv_hero);
        ivHero.setImageResource(hero.getRessourceImage());

        TextView tvHeroName = (TextView) convertView.findViewById(R.id.tv_hero);
        tvHeroName.setTypeface(NoNavigationActivity.bodyFontBold);
        tvHeroName.setText(hero.getName());

        ProgressBar pbLife = (ProgressBar) convertView.findViewById(R.id.pb_life);
        pbLife.setProgress((int)(hero.getEndurance()*1.0/hero.getBaseEndurance()*100));

        TextView tvLife = (TextView) convertView.findViewById(R.id.tv_life);
        tvLife.setTypeface(NoNavigationActivity.bodyFontBold);
        tvLife.setText(hero.getEndurance() + "/" + hero.getBaseEndurance());

        return convertView;
    }

    public AdapterView.OnItemLongClickListener changeOrder(){
        return new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if(selectedHeros == -1){
                    selectedHeros = position;
                }
                else{
                    selectedHeros = -1;
                }
                return true;
            }
        };
    }

    public AdapterView.OnItemClickListener inventoryListener(){

        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(selectedHeros == -1){
                    Intent intent = new Intent(getContext(), InventoryActivity.class);
                    intent.putExtra("selected_hero", position);
                    getContext().startActivity(intent);
                }
                else{
                    Game.getInstance().setOrder(selectedHeros, position);
                    gv_heroes.setAdapter(PlayableActorAdapter.this);
                    selectedHeros = -1;
                }
            }
        };
    }
}
