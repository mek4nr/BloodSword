package com.unidev.bloodsword.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.SeekBar;
import android.widget.TextView;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.models.Game;
import com.unidev.bloodsword.models.actors.Actor;
import com.unidev.bloodsword.models.actors.playableActors.PlayableActor;

import java.util.HashMap;

public class HealAdapter extends ArrayAdapter<PlayableActor> {

    @LayoutRes int layout;
    int maxHeal, currentHeal;
    HashMap<Actor,Integer> healed = new HashMap<>();

    public HealAdapter(@NonNull Context context, @LayoutRes int resource, int maxHeal) {
        super(context, 0, Game.getInstance().getCharacters());
        layout = resource;
        currentHeal = 0;
        this.maxHeal = maxHeal;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final PlayableActor current_actor = getItem(position);
        healed.put(current_actor,0);
        if(convertView == null){
            LayoutInflater inflator = LayoutInflater.from(getContext());
            convertView = inflator.inflate(layout,null);
        }

        ImageView iv_hero_heal = (ImageView) convertView.findViewById(R.id.iv_hero_heal);
        TextView tv_hero_heal_name = (TextView) convertView.findViewById(R.id.tv_hero_heal_name);
        SeekBar sb_life_heal = (SeekBar) convertView.findViewById(R.id.sb_life_heal);
        final TextView tv_life_heal = (TextView) convertView.findViewById(R.id.tv_life_heal);

        iv_hero_heal.setImageResource(current_actor.getImageResource());
        tv_hero_heal_name.setText(current_actor.getName());

        tv_life_heal.setText("" + current_actor.getEndurance());

        sb_life_heal.setMax(Math.min(maxHeal,current_actor.getBaseEndurance()-current_actor.getEndurance()));
        sb_life_heal.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            private int baseProgress;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if( currentHeal + progress-baseProgress > maxHeal){
                    seekBar.setProgress(baseProgress + maxHeal-currentHeal);
                }
                tv_life_heal.setText("" + (current_actor.getEndurance() + seekBar.getProgress()));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                baseProgress = seekBar.getProgress();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                currentHeal += seekBar.getProgress() - baseProgress;
                healed.put(HealAdapter.this.getItem(position),seekBar.getProgress());
            }
        });

        return convertView;
    }

    public HashMap<Actor, Integer> getHealed() {
        return healed;
    }
}
