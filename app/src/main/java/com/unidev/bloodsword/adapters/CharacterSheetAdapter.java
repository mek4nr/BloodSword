package com.unidev.bloodsword.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.managers.InventoryManager;
import com.unidev.bloodsword.models.actors.playableActors.PlayableActor;

import java.util.List;

public class CharacterSheetAdapter extends ArrayAdapter<PlayableActor> {

    private int layout;
    private Context context;

    public CharacterSheetAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<PlayableActor> objects) {
        super(context, 0, objects);
        this.layout = resource;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        PlayableActor hero = getItem(position);

        if(convertView == null){
            LayoutInflater inflator = LayoutInflater.from(getContext());
            convertView = inflator.inflate(layout,null);
        }

        ImageView iv_hero_sheet = (ImageView) convertView.findViewById(R.id.iv_hero_sheet);
        TextView tv_hero_sheet  = (TextView) convertView.findViewById(R.id.tv_hero_sheet);
        GridView gv_hero_sheet = (GridView) convertView.findViewById(R.id.gv_hero_sheet);

        iv_hero_sheet.setImageResource(hero.getImageResource());
        tv_hero_sheet.setText(hero.getName() +
                "\nEND : " + hero.getEndurance() +
                "\nF.P : " + hero.getFightingProwess() +
                "\nAWA : " + hero.getAwareness() +
                "\nP.A : " + hero.getPsychicAbility()
        );

        gv_hero_sheet.setAdapter(new InventoryAdapter(context, R.layout.widget_item, InventoryManager.getInventory(hero)));

        return convertView;
    }
}
