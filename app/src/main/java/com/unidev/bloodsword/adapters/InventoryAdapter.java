package com.unidev.bloodsword.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.models.inventory.Item;
import com.unidev.bloodsword.models.inventory.StorageItem;

public class InventoryAdapter extends ArrayAdapter<Item> {

    private int layout;

    public InventoryAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull Item[] objects) {
        super(context, 0, objects);
        this.layout = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Item item = getItem(position);

        if(convertView == null){
            LayoutInflater inflator = LayoutInflater.from(getContext());
            convertView = inflator.inflate(layout,null);
        }

        TextView tv_item_name = (TextView) convertView.findViewById(R.id.tv_item_name);
        if(item != null)
            tv_item_name.setText(item.getName());

        TextView tv_item_quantity = (TextView) convertView.findViewById(R.id.tv_item_quantity);
        if(item instanceof StorageItem){
            tv_item_quantity.setText(((StorageItem) item).getItemAccepted().getName() + "x" + ((StorageItem) item).getQuantity());
        }

        return convertView;
    }
}
