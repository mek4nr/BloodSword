package com.unidev.bloodsword.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.activities.NoNavigationActivity;
import com.unidev.bloodsword.widgets.ChoseHeroWidget;
import com.unidev.bloodsword.widgets.ToggleButtonOldScroll;

import java.util.List;

import static com.unidev.bloodsword.activities.NoNavigationActivity.bodyFontBold;
import static com.unidev.bloodsword.activities.NoNavigationActivity.bodyFontRegular;

public class ChoseHeroAdapter extends ArrayAdapter<ChoseHeroWidget> {
    @LayoutRes private int layout;
    private List<ChoseHeroWidget> heroes;

    public ChoseHeroAdapter(@NonNull Context context, @LayoutRes int resource, List<ChoseHeroWidget> objects) {
        super(context, 0, objects);
        this.layout = resource;
        this.heroes = objects;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ChoseHeroWidget hero = getItem(position);

        if(convertView == null){
            LayoutInflater inflator = LayoutInflater.from(getContext());
            convertView = inflator.inflate(layout,null);
        }

        TextView tv_hero_class = (TextView) convertView.findViewById(R.id.tv_hero_class);
        ImageView ivHero = (ImageView) convertView.findViewById(R.id.iv_hero);
        final EditText etHero = (EditText) convertView.findViewById(R.id.et_hero);
        final ToggleButtonOldScroll tgHero = (ToggleButtonOldScroll) convertView.findViewById(R.id.tb_chosen);

        tv_hero_class.setText(hero.characterType.toString());
        tv_hero_class.setTypeface(bodyFontBold);
        ivHero.setImageResource(hero.image);
        etHero.setHint(hero.default_name);
        etHero.setTypeface(bodyFontRegular);
        tgHero.setChecked(hero.activated);
        tgHero.setTypeface(bodyFontRegular);

        tgHero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                heroes.get(position).activated = tgHero.isChecked();
            }
        });

        etHero.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                heroes.get(position).name = etHero.getText().toString();
            }
        });

        return convertView;
    }
}