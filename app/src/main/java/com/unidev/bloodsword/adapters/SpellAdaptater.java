package com.unidev.bloodsword.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.models.battle.Tile;
import com.unidev.bloodsword.models.spells.BlastingSpell;
import com.unidev.bloodsword.models.spells.Spell;

import java.util.List;

/**
 * Created by JB on 23/03/2017.
 */

public class SpellAdaptater extends ArrayAdapter<Spell> {

    @LayoutRes private int layout;


    public SpellAdaptater(@NonNull Context context, @LayoutRes int resource, @NonNull List<Spell> objects) {
        super(context, 0, objects);
        layout = resource;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Spell current_spell = getItem(position);

        if(convertView == null){
            LayoutInflater inflator = LayoutInflater.from(getContext());
            convertView = inflator.inflate(layout,null);
        }

        TextView tv_spell_name = (TextView) convertView.findViewById(R.id.tv_spell_name);
        TextView tv_spell_type = (TextView) convertView.findViewById(R.id.tv_spell_type);
        TextView tv_spell_level = (TextView) convertView.findViewById(R.id.tv_spell_level);
        TextView tv_spell_description = (TextView) convertView.findViewById(R.id.tv_spell_description);

        tv_spell_name.setText(current_spell.getName());
        if(current_spell instanceof BlastingSpell){
            tv_spell_type.setText(R.string.tv_type_blasting_text);
        }
        else{
            tv_spell_type.setText(R.string.tv_type_psychic_text);
        }
        tv_spell_level.setText("" + current_spell.getComplexity());
        tv_spell_description.setText(current_spell.getDescription());

        return convertView;
    }
}
