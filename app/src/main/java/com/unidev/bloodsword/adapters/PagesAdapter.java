package com.unidev.bloodsword.adapters;


import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.models.actors.ECharacterType;
import com.unidev.bloodsword.models.book.Choice;
import com.unidev.bloodsword.models.book.Page;

import java.util.ArrayList;

public class PagesAdapter extends ArrayAdapter {
    // Attributs
    private ArrayList<Page> pages;

    // Constructeurs
    public PagesAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }

    // Accesseurs

    public ArrayList<Page> getPages() {
        return pages;
    }

    // Mutateurs

    public void setPages(ArrayList<Page> pages) {
        this.pages = pages;
    }
}
