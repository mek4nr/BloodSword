package com.unidev.bloodsword.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.models.EActionType;
import com.unidev.bloodsword.models.actors.Actor;
import com.unidev.bloodsword.models.actors.enemyActors.EnemyActor;
import com.unidev.bloodsword.models.battle.Battlefield;
import com.unidev.bloodsword.models.battle.NormalTile;
import com.unidev.bloodsword.models.battle.Tile;
import com.unidev.bloodsword.models.battle.WallTile;
import com.unidev.bloodsword.utils.Sprite;

public class BattlefieldAdapter extends ArrayAdapter<Tile>{

    @LayoutRes private int layout;
    private Battlefield battlefield;
    private boolean applyRedBox = false;
    private final EActionType[] redBoxActions = {EActionType.SHOOT, EActionType.FIGHT_MOVE, EActionType.CAST_SPELL};

    public BattlefieldAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull Battlefield battlefield) {
        super(context, 0, battlefield.getListTiles());
        this.layout = resource;
        this.battlefield = battlefield;
        this.battlefield.generateOrder();
    }

    private void generateTile(Tile tile, FrameLayout flTile, ImageView ivBackground, ImageView ivActor){
        ivBackground.setBackgroundResource(0);

        if(tile instanceof WallTile){
            ivBackground.setBackgroundColor(Color.BLACK);
            flTile.setBackgroundColor(Color.WHITE);
        }
        else if(tile instanceof NormalTile){
            ivBackground.setBackgroundColor(Color.WHITE);
            flTile.setBackgroundColor(Color.BLACK);
        }
    }

    private void generateActor(Tile tile, FrameLayout flTile, ImageView ivBackground, ImageView ivActor){
        if(tile.getActor() != null){
            if(tile.getActor() == battlefield.actorPlaying()){
                if(tile.getActor() instanceof EnemyActor){
                    ivBackground.setBackgroundResource(R.color.colorBackEnnemy);
                }
                else{
                    ivBackground.setBackgroundResource(R.color.colorBackPlayer);
                }
            }
            if(tile.getActor().isFleeing()){
                ivBackground.setBackgroundResource(R.color.colorBackAction);
            }

            if(tile.getActor() == battlefield.getTarget()){
                ivBackground.setBackgroundResource(R.color.colorBackTarget);
            }

            if(applyRedBox && battlefield.isPossibleTarget(tile.getActor()))
            {
                flTile.setBackgroundResource(R.color.colorBackTarget);
            }

            Sprite sprite;
            if((sprite = tile.getActor().getSprite()) != null) {
                ivActor.setImageResource(sprite.getImageIddle());
            }


        }
        else{
            ivActor.setImageResource(0);
        }
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Tile current_tile = getItem(position);

        LayoutInflater inflator = LayoutInflater.from(getContext());
        convertView = inflator.inflate(layout,null);

        FrameLayout flTile = (FrameLayout) convertView.findViewById(R.id.fl_tile);
        ImageView ivBackground = (ImageView) convertView.findViewById(R.id.iv_background);
        ImageView ivActor = (ImageView) convertView.findViewById(R.id.iv_actor);

        ivBackground.setAlpha(0);
        generateTile(current_tile, flTile, ivBackground, ivActor);
        generateActor(current_tile, flTile, ivBackground, ivActor);

        return convertView;
    }

    public void updatePlayingActor() {
        this.notifyDataSetChanged();
    }

    public void  updateAction(){
        applyRedBox = false;

        for(EActionType action : redBoxActions){
            if (battlefield.getAction() == action) {
                if((battlefield.getAction() == EActionType.CAST_SPELL && battlefield.getSpell() != null)
                    || battlefield.getAction() != EActionType.CAST_SPELL){
                    applyRedBox = true;
                }
                break;
            }
        }
        this.notifyDataSetChanged();
    }

    public void updateTarget(){
        this.notifyDataSetChanged();
    }
}
