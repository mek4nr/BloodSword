package com.unidev.bloodsword.widgets;

import android.content.Context;
import android.util.AttributeSet;

import com.unidev.bloodsword.R;

import static com.unidev.bloodsword.activities.NoNavigationActivity.bodyFontBoldItalic;
import static com.unidev.bloodsword.activities.NoNavigationActivity.bodyFontItalic;

/**
 * Created by guill on 25/03/2017.
 */

public class ButtonOldScroll extends android.support.v7.widget.AppCompatButton {

    public ButtonOldScroll(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBackgroundResource(R.drawable.button_scroll);
        setTypeface(bodyFontBoldItalic);
        setPadding(200, 50, 200, 40);
    }

    public ButtonOldScroll(Context context, AttributeSet attrs, Boolean bBook) {
        super(context, attrs);

        setBackgroundResource(R.drawable.button_scroll);
        setTypeface(bodyFontItalic);
        if(bBook) {
            setPadding(200, 120, 200, 110);
        } else {
            setPadding(200, 50, 200, 40);
        }


    }
}
