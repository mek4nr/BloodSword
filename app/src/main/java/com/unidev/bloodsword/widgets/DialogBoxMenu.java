package com.unidev.bloodsword.widgets;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.activities.MenuHomeActivity;
import com.unidev.bloodsword.activities.TitleActivity;
import com.unidev.bloodsword.models.EDifficulty;
import com.unidev.bloodsword.models.Game;

import static com.unidev.bloodsword.activities.NoNavigationActivity.bodyFontBold;

public class DialogBoxMenu extends Dialog implements View.OnClickListener {

    // Attributs
    private Context ctx;
    private TextView tv_db_menu_title;
    private FrameLayout fl_db_menu;
    private ButtonOldScroll btn_db_back_to_game, btn_db_menu_save, btn_db_menu_home, btn_db_menu_save_home, btn_db_menu_save_exit, btn_db_menu_exit;

    // Constructeur
    public DialogBoxMenu(@NonNull Context context) {
        super(context);
        this.ctx = context;
    }

    // Surcharges
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fl_db_menu = (FrameLayout) findViewById(R.id.fl_db_menu);
        setContentView(R.layout.dialog_save_menu);
        tv_db_menu_title = (TextView) findViewById(R.id.tv_db_menu_title);
        tv_db_menu_title.setTypeface(bodyFontBold);

        btn_db_back_to_game = (ButtonOldScroll) findViewById(R.id.btn_back_to_game);
        btn_db_menu_save = (ButtonOldScroll) findViewById(R.id.btn_db_menu_save);
        btn_db_menu_home = (ButtonOldScroll) findViewById(R.id.btn_db_menu_home);
        btn_db_menu_save_home = (ButtonOldScroll) findViewById(R.id.btn_db_menu_save_home);
        btn_db_menu_save_exit = (ButtonOldScroll) findViewById(R.id.btn_db_menu_save_exit);
        btn_db_menu_exit = (ButtonOldScroll) findViewById(R.id.btn_db_menu_exit);

        btn_db_back_to_game.setOnClickListener(this);
        btn_db_menu_save.setOnClickListener(this);
        btn_db_menu_home.setOnClickListener(this);
        btn_db_menu_save_home.setOnClickListener(this);
        btn_db_menu_save_exit.setOnClickListener(this);
        btn_db_menu_exit.setOnClickListener(this);

        if(Game.getInstance().getDifficulty() == EDifficulty.SURVIVAL){
            btn_db_menu_save.setEnabled(false);
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch(v.getId()){
            case R.id.btn_db_menu_save:
                Game.getInstance().saveGame(ctx);
                dismiss();
                break;
            case R.id.btn_db_menu_home:
                Intent menu_home = new Intent(ctx, MenuHomeActivity.class);
                ctx.startActivity(menu_home);
                break;
            case R.id.btn_db_menu_save_home:
                Game.getInstance().saveGame(ctx);
                intent = new Intent(ctx, MenuHomeActivity.class);
                ctx.startActivity(intent);
                break;
            case R.id.btn_db_menu_save_exit:
                Game.getInstance().saveGame(ctx);
                intent = new Intent(ctx, TitleActivity.class);
                ctx.startActivity(intent);
                ((Activity) ctx).finish();
                ((Activity) ctx).moveTaskToBack(true);
                break;
            case R.id.btn_db_menu_exit:
                intent = new Intent(ctx, TitleActivity.class);
                ctx.startActivity(intent);
                ((Activity) ctx).finish();
                ((Activity) ctx).moveTaskToBack(true);
                break;
            case R.id.btn_back_to_game:
                dismiss();
        }
    }
}
