package com.unidev.bloodsword.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ToggleButton;

import com.unidev.bloodsword.R;

import static com.unidev.bloodsword.activities.NoNavigationActivity.bodyFontBold;

/**
 * Created by guill on 26/03/2017.
 */

public class ToggleButtonOldScroll extends ToggleButton {

    public ToggleButtonOldScroll(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBackgroundResource(R.drawable.button_scroll);
        setTypeface(bodyFontBold);
        setPadding(100, 30, 100, 20);
    }
}
