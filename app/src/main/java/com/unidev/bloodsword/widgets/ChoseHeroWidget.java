package com.unidev.bloodsword.widgets;

import com.unidev.bloodsword.models.actors.ECharacterType;

public final class ChoseHeroWidget {
    public final ECharacterType characterType;
    public final int image;
    public final String default_name;
    public String name;

    public boolean activated = false;

    public ChoseHeroWidget(String name, ECharacterType characterType, int image) {
        this.name = name;
        this.default_name = name;
        this.characterType = characterType;
        this.image = image;
    }

    @Override
    public String toString() {
        return "ChoseHeroWidget{" +
                "characterType=" + characterType +
                ", name='" + name + '\'' +
                ", activated=" + activated +
                '}';
    }
}
