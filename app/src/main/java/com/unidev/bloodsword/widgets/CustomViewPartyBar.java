package com.unidev.bloodsword.widgets;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.unidev.bloodsword.R;
import com.unidev.bloodsword.adapters.PlayableActorAdapter;
import com.unidev.bloodsword.models.Game;

public class CustomViewPartyBar extends LinearLayout {

    // Attributs
    private ImageButton btn_party_menu;
    private GridView gv_heroes;
    PlayableActorAdapter playableActorAdapter;

    Game game = Game.getInstance();

    // Constructeur
    public CustomViewPartyBar(final Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        // Configuration de l'apparence générale de la vue personnalisée
        setOrientation(HORIZONTAL);
        setBackgroundColor(Color.BLACK);
        setGravity(Gravity.CENTER);
        setPadding(0,0,0,10);
        setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        // Création des boutons des personnages
        gv_heroes = new GridView(context);
        gv_heroes.setNumColumns(4);
        gv_heroes.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 10));
        playableActorAdapter = new PlayableActorAdapter(context,R.layout.widget_hero, game.getCharacters());
        playableActorAdapter.gv_heroes = gv_heroes;
        gv_heroes.setAdapter(playableActorAdapter);
        gv_heroes.setOnItemClickListener(playableActorAdapter.inventoryListener());
        gv_heroes.setOnItemLongClickListener(playableActorAdapter.changeOrder());

        // Création du bouton de menu
        btn_party_menu = new ImageButton(context);
        btn_party_menu.setImageResource(R.drawable.button_menu);
        btn_party_menu.setBackgroundColor(Color.TRANSPARENT);
        btn_party_menu.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1));
        btn_party_menu.setPadding(10,0,10,0);
        btn_party_menu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogBoxMenu dbMenu = new DialogBoxMenu(context);
                dbMenu.show();
            }
        });

        // Ajout des boutons à la vue personnalisée
        addView(gv_heroes);
        addView(btn_party_menu);
    }

    public void onHeroesChanged(){
        playableActorAdapter.notifyDataSetChanged();
        gv_heroes.setAdapter(playableActorAdapter);
    }
}
